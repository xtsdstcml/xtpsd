package test.xtcoder.tools.xtpsd;

import java.io.File;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.psd.PsdImageParser;
import org.junit.Test;

public class TestImage {
	@Test
	public void testPsdParse() {
		String psdFilePath = "src/test/resources/img01.psd";
		try {
            File psdFile = new File(psdFilePath);

            // 使用 PSDImageParser 解析 PSD 文件
            PsdImageParser parser = new PsdImageParser();
            ImageInfo imageInfo = Imaging.getImageInfo(psdFile);
            ImageMetadata metadata = Imaging.getMetadata(psdFile);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
