package test.xtcoder.tools.xtpsd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.psd.PsdImageParser;
import org.junit.Test;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import top.xtcoder.xtpsd.PSD;
import top.xtcoder.xtpsd.core.PsdInfo;
import top.xtcoder.xtpsd.core.layermask.lang.BlendMode;
import top.xtcoder.xtpsd.info.InfoExt;
import top.xtcoder.xtpsd.info.PsdInfoExt;

public class TestPsd {
	
	@Test
	public void testReadPsd() throws FileNotFoundException, IOException {
		try(FileInputStream fis = new FileInputStream("src/test/resources/img.psd");){
			byte[] res = new byte[50];
			fis.read(res);
			System.out.println(Arrays.toString(res));
		}
	}
	
	@Test
	public void testByte(){
		System.out.println(BlendMode.bm("pass"));
	}
	
	@Test
	public void testPsdParseExt() {
		try {
			PSD psd = new PSD("src/test/resources/img01.psd");
			PsdInfo info = psd.parse();
			PsdInfoExt ext = InfoExt.to(info);
			FileUtil.writeUtf8String(JSONUtil.toJsonPrettyStr(info), "C:\\Users\\xiangtao\\Desktop\\psd.json");
			FileUtil.writeUtf8String(JSONUtil.toJsonPrettyStr(ext), "C:\\Users\\xiangtao\\Desktop\\psd_ext.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testPsdParse() {
		try {
			PSD psd = new PSD("src/test/resources/img01.psd");
			PsdInfo info = psd.parse();
			info.getImageResources().getImageResources().forEach(item -> {
//				System.out.println("signature=" + item.getSignature() 
//					+ ",id=" + item.getImageResourceID()
//					+ ",name=" + item.getName() 
//					+ ", resourceDataLength=" + item.getResourceDataLength());
			});
			
			FileUtil.writeUtf8String(JSONUtil.toJsonPrettyStr(info), "C:\\Users\\xiangtao\\Desktop\\psd.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testParsePSDHeader() {
		try {
            FileInputStream fis = new FileInputStream("F:\\workspace\\sts\\my\\xtpsd\\src\\test\\resources\\img01.psd");
            // 读取文件头部信息
            byte[] header = new byte[26];
            fis.read(header);
            // 解析文件头部信息
            int version = header[0];
            int channels = ((header[11] & 0xFF) << 8) | (header[12] & 0xFF);
            int width = ((header[13] & 0xFF) << 8) | (header[14] & 0xFF);
            int height = ((header[15] & 0xFF) << 8) | (header[16] & 0xFF);
            int bitsPerChannel = (header[19] & 0xFF);
            // 输出解析结果
            System.out.println("Version: " + version);
            System.out.println("Channels: " + channels);
            System.out.println("Width: " + width);
            System.out.println("Height: " + height);
            System.out.println("Bits per Channel: " + bitsPerChannel);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	@Test
	public void testMd5() {
		long t1 = System.currentTimeMillis();
		for(long s = 0; s <= 10000000; s ++) {
			SecureUtil.md5(s + "");
		}
		long t2 = System.currentTimeMillis();
		System.out.println(t2 - t1);
	}
	
	@Test
	public void testCommonImage() {
		String psdFilePath = "src/test/resources/img02.psd";
		try {
            File psdFile = new File(psdFilePath);

            // 使用 PSDImageParser 解析 PSD 文件
            PsdImageParser parser = new PsdImageParser();
            ImageInfo imageInfo = Imaging.getImageInfo(psdFile);
            ImageMetadata metadata = Imaging.getMetadata(psdFile);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	@Test
	public void testNashorn() {
		String psdFilePath = "src/test/resources/img01.psd";
        String javascriptFilePath = "src/test/resources/psd-parser.js";

        try {
            // 创建 JavaScript 引擎
            ScriptEngineManager engineManager = new ScriptEngineManager();
            ScriptEngine engine = engineManager.getEngineByName("nashorn");

            // 执行 JavaScript 文件
            engine.eval("src/test/resources/psd.js");
            //engine.eval(new FileReader(javascriptFilePath));

            // 获取 JavaScript 函数
            Invocable invocable = (Invocable) engine;
            Object result = invocable.invokeFunction("parsePSD", psdFilePath);

            // 输出图层和滤镜信息
            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}