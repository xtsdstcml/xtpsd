package top.xtcoder.xtpsd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import top.xtcoder.xtpsd.core.PsdInfo;
import top.xtcoder.xtpsd.core.color.mode.data.ColorModelDataParse;
import top.xtcoder.xtpsd.core.color.mode.data.vo.PsdColorModeData;
import top.xtcoder.xtpsd.core.file.header.FileHeaderParse;
import top.xtcoder.xtpsd.core.file.header.vo.PsdFileHeader;
import top.xtcoder.xtpsd.core.image.resources.ImageResourceParse;
import top.xtcoder.xtpsd.core.image.resources.vo.PsdImageResources;
import top.xtcoder.xtpsd.core.layermask.PsdLayerAndMaskInfomationParse;
import top.xtcoder.xtpsd.core.layermask.vo.PsdLayerAndMaskInfomation;

public class PSD {
	private String path;
	
	public PSD(String psdFilePath) throws FileNotFoundException {
		this.path = psdFilePath;
	}
	
	public PsdInfo parse() throws Exception{
		try(FileInputStream fis = new FileInputStream(this.path);){
			PsdInfo psdInfo = new PsdInfo();
			
			psdInfo.setFileHeader(parseFileHeader(fis));
			psdInfo.setColorModeData(parseColorModel(fis));
			psdInfo.setImageResources(parseImageResource(fis));
			psdInfo.setLayerAndMaskInfomation(parseLayerAndMaskInfo(fis));
			
			return psdInfo;
		}
	}
	
	/**
	 * 解析文件头
	 * @param fis
	 * @return
	 * @throws IOException
	 */
	private PsdFileHeader parseFileHeader(FileInputStream fis) throws IOException {
		FileHeaderParse parse = new FileHeaderParse();
		return parse.parse(fis);
	}
	
	/**
	 * 解析颜色映射表
	 * @param fis
	 * @return
	 * @throws IOException
	 */
	private PsdColorModeData parseColorModel(FileInputStream fis) throws IOException {
		ColorModelDataParse parse = new ColorModelDataParse();
		return parse.parse(fis);
	}
	
	/**
	 * 解析图像资源
	 * @param fis
	 * @return
	 * @throws IOException
	 */
	private PsdImageResources parseImageResource(FileInputStream fis) throws IOException {
		ImageResourceParse parse = new ImageResourceParse();
		return parse.parse(fis);
	}
	
	/**
	 * 解析图层和蒙版信息
	 * @param fis
	 * @return
	 * @throws IOException
	 */
	private PsdLayerAndMaskInfomation parseLayerAndMaskInfo(FileInputStream fis) throws IOException {
		PsdLayerAndMaskInfomationParse parse = new PsdLayerAndMaskInfomationParse();
		return parse.parse(fis);
	}
}
