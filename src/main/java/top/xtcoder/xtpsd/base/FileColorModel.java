package top.xtcoder.xtpsd.base;

public enum FileColorModel {
	Bitmap(0), 
	Grayscale(1),
	Indexed(2),
	RGB(3),
	CMYK(4), 
	Multichannel(7), 
	Duotone(8), 
	Lab(9);
	
	int value;
	FileColorModel(int v) {
		this.value = v;
	}
}
