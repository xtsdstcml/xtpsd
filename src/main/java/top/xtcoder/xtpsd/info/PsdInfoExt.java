package top.xtcoder.xtpsd.info;

import java.util.List;

public class PsdInfoExt {
	private int channelsNumber;
	private int width;
	private int height;
	private int depth;
 	private String colorModel;
 	private int layerCount;
 	
 	private List<Layer> layers;
 	
	public int getChannelsNumber() {
		return channelsNumber;
	}
	public void setChannelsNumber(int channelsNumber) {
		this.channelsNumber = channelsNumber;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public String getColorModel() {
		return colorModel;
	}
	public void setColorModel(String colorModel) {
		this.colorModel = colorModel;
	}
	public int getLayerCount() {
		return layerCount;
	}
	public void setLayerCount(int layerCount) {
		this.layerCount = layerCount;
	}
	public List<Layer> getLayers() {
		return layers;
	}
	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}
}
