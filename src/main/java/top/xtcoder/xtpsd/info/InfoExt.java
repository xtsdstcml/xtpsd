package top.xtcoder.xtpsd.info;

import java.util.ArrayList;
import java.util.List;

import top.xtcoder.xtpsd.core.PsdInfo;
import top.xtcoder.xtpsd.core.layermask.vo.LayerEffect;
import top.xtcoder.xtpsd.core.layermask.vo.LayerRecord;

public class InfoExt {
	public static PsdInfoExt to(PsdInfo psdInfo) {
		PsdInfoExt ext = new PsdInfoExt();
		if(psdInfo == null) {
			return ext;
		}
		if(psdInfo.getFileHeader() != null) {
			ext.setChannelsNumber(psdInfo.getFileHeader().getChannelsNumber());
			ext.setColorModel(psdInfo.getFileHeader().getColorModel().name());
			ext.setDepth(psdInfo.getFileHeader().getDepth());
			ext.setWidth(psdInfo.getFileHeader().getWidth());
			ext.setHeight(psdInfo.getFileHeader().getHeight());
		}
		if(psdInfo.getLayerAndMaskInfomation() != null && psdInfo.getLayerAndMaskInfomation().getLayerInfo() != null) {
			ext.setLayerCount(psdInfo.getLayerAndMaskInfomation().getLayerInfo().getLayerCount());
			List<Layer> layers = new ArrayList<Layer>();
			for(int i = 0; psdInfo.getLayerAndMaskInfomation().getLayerInfo().getRecords() != null && i < psdInfo.getLayerAndMaskInfomation().getLayerInfo().getRecords().size(); i ++) {
				LayerRecord layerRecord = psdInfo.getLayerAndMaskInfomation().getLayerInfo().getRecords().get(i);
				Layer layer = to(layerRecord);
				layers.add(layer);
			}
			ext.setLayers(layers);
		}
		
		return ext;
	}
	
	private static Layer to(LayerRecord record) {
		Layer layer = new Layer();
		if(record.getBlendMode() != null) {
			layer.setBlendMode(record.getBlendMode().getMode().toString());
		}
		if(record.getChannelInformation() != null) {
			layer.setChannelNumber(record.getChannelInformation().getChannelNumber());
		}
		layer.setClipping(record.getClipping());
		layer.setCoordinate(record.getCoordinate());
		layer.setFiller(record.getFiller());
		
		if(record.getFlag() != null) {
			layer.setDocumentPixData(record.getFlag().getDocumentPixData());
			layer.setObsolete(record.getFlag().getObsolete());
			layer.setOpacity(record.getFlag().getObsolete());
			layer.setTransparencyProtected(record.getFlag().getTransparencyProtected());
			layer.setUseful4(record.getFlag().getUseful4());
			layer.setVisible(record.getFlag().getVisible());
		}
		if(record.getNameInfo() != null) {
			layer.setName(record.getNameInfo().getName());
		}
		List<LayerEffect> effects = record.getEffects();
		layer.setEffects(effects);
		return layer;
	}
}
