package top.xtcoder.xtpsd.info;

import java.util.List;

import top.xtcoder.xtpsd.core.layermask.vo.LayerEffect;
import top.xtcoder.xtpsd.core.layermask.vo.Location;

public class Layer {
	private Location coordinate;
	private int channelNumber;
	private String blendMode;
	private int opacity;
	private int clipping;
	private int transparencyProtected;
	private int visible;
	private int obsolete;
	private int useful4;
	private int documentPixData;
	private int filler;
	private String name;
	
	private List<LayerEffect> effects;
	
	public Location getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Location coordinate) {
		this.coordinate = coordinate;
	}
	public int getChannelNumber() {
		return channelNumber;
	}
	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}
	public String getBlendMode() {
		return blendMode;
	}
	public void setBlendMode(String blendMode) {
		this.blendMode = blendMode;
	}
	public int getOpacity() {
		return opacity;
	}
	public void setOpacity(int opacity) {
		this.opacity = opacity;
	}
	public int getClipping() {
		return clipping;
	}
	public void setClipping(int clipping) {
		this.clipping = clipping;
	}
	public int getTransparencyProtected() {
		return transparencyProtected;
	}
	public void setTransparencyProtected(int transparencyProtected) {
		this.transparencyProtected = transparencyProtected;
	}
	public int getVisible() {
		return visible;
	}
	public void setVisible(int visible) {
		this.visible = visible;
	}
	public int getObsolete() {
		return obsolete;
	}
	public void setObsolete(int obsolete) {
		this.obsolete = obsolete;
	}
	public int getUseful4() {
		return useful4;
	}
	public void setUseful4(int useful4) {
		this.useful4 = useful4;
	}
	public int getDocumentPixData() {
		return documentPixData;
	}
	public void setDocumentPixData(int documentPixData) {
		this.documentPixData = documentPixData;
	}
	public int getFiller() {
		return filler;
	}
	public void setFiller(int filler) {
		this.filler = filler;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<LayerEffect> getEffects() {
		return effects;
	}
	public void setEffects(List<LayerEffect> effects) {
		this.effects = effects;
	}
}
