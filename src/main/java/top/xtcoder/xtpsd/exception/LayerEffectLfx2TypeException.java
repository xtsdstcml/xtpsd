package top.xtcoder.xtpsd.exception;

public class LayerEffectLfx2TypeException  extends RuntimeException{
	public LayerEffectLfx2TypeException(String msg) {
		super(msg);
	}
}