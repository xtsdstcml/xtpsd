package top.xtcoder.xtpsd.exception;

public class EffectSetNotFoundException extends RuntimeException{
	public EffectSetNotFoundException(String msg) {
		super(msg);
	}
}
