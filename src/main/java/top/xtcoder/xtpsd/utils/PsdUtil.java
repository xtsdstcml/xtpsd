package top.xtcoder.xtpsd.utils;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PsdUtil {
	/**
	 * num补全为completion的倍数
	 * @return
	 */
	public static int completionMultiple(int num, int completion) {
		while(num % completion != 0) {
			num ++;
		}
		return num;
	}
}
