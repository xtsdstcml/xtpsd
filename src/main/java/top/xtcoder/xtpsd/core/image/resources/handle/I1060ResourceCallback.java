package top.xtcoder.xtpsd.core.image.resources.handle;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class I1060ResourceCallback implements IResourceCallback{
	private static final int resourceId = 1000;
	
	@Override
	public Map<String, Object> handle(byte[] data) {
		Map<String, Object> map = new HashMap<>();
		map.put("xmpMetadata", new String(data));
		return null;
	}

}
