package top.xtcoder.xtpsd.core.image.resources.vo;

import java.util.Map;

public class ImageResource {
	private String signature;
	
	private int imageResourceID;
	
	private long nameLength;
	
	private String name;
	
	private long resourceDataLength; 
	
	private Map<String, Object> resourceData;
	
	private byte[] resourceDataEnd;
	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public int getImageResourceID() {
		return imageResourceID;
	}
	public void setImageResourceID(int imageResourceID) {
		this.imageResourceID = imageResourceID;
	}
	public long getNameLength() {
		return nameLength;
	}
	public void setNameLength(long nameLength) {
		this.nameLength = nameLength;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public long getResourceDataLength() {
		return resourceDataLength;
	}
	public void setResourceDataLength(long resourceDataLength) {
		this.resourceDataLength = resourceDataLength;
	}
	
	public Map<String, Object> getResourceData() {
		return resourceData;
	}
	public void setResourceData(Map<String, Object> resourceData) {
		this.resourceData = resourceData;
	}
	public byte[] getResourceDataEnd() {
		return resourceDataEnd;
	}
	public void setResourceDataEnd(byte[] resourceDataEnd) {
		this.resourceDataEnd = resourceDataEnd;
	}
	
}
