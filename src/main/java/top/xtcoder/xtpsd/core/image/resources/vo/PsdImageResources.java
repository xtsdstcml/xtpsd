package top.xtcoder.xtpsd.core.image.resources.vo;

import java.util.List;

public class PsdImageResources {
	private int imageResourceLength;
	
	private List<ImageResource> imageResources;
	
	public int getImageResourceLength() {
		return imageResourceLength;
	}

	public void setImageResourceLength(int imageResourceLength) {
		this.imageResourceLength = imageResourceLength;
	}

	public List<ImageResource> getImageResources() {
		return imageResources;
	}

	public void setImageResources(List<ImageResource> imageResources) {
		this.imageResources = imageResources;
	}
	
}
