package top.xtcoder.xtpsd.core.image.resources.handle;

import java.util.Map;

public interface IResourceCallback {
	Map<String, Object> handle(byte[] data);
	
}
