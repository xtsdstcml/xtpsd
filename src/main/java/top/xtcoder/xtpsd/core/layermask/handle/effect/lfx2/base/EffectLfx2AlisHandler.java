package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 数据型对象
 * 
 * {
 * 		id="?",	
 * 		type='alis',		// 对象类型(char)，总是为'alis',4字节
 * 		length=?,			// 数据长度(int),4字节
 * 		data=".."			// 数据,1字节*数据长度
 * }
 * @author xiangtao
 *
 */
public class EffectLfx2AlisHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int dataLength = ByteUtil.readByteToInt(inputStream, 4);
		String data = ByteUtil.readByteToStr(inputStream, dataLength);
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("dataLength", dataLength);
		map.put("data", data);
		return map;
	}

}
