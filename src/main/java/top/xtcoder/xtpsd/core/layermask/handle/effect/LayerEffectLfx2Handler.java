package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2EbblHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2GrFlHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2IrGlHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2MasterFXSwitchHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2OrGlHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2SciHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.EffectLfx2SoFiHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectChFXHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectDrShHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectFrFXHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectIrShHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectPatternFillHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectLfx2Handler implements ILayerEffectHandler{
	private static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.Scl.name(), new EffectLfx2SciHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.masterFXSwitch.name(), new EffectLfx2MasterFXSwitchHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.OrGl.name(), new EffectLfx2OrGlHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.SoFi.name(), new EffectLfx2SoFiHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.IrGl.name(), new EffectLfx2IrGlHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.ebbl.name(), new EffectLfx2EbblHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.GrFl.name(), new EffectLfx2GrFlHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.ChFX.name(), new EffectChFXHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.DrSh.name(), new EffectDrShHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.IrSh.name(), new EffectIrShHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.patternFill.name(), new EffectPatternFillHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.FrFX.name(), new EffectFrFXHandler());
	}
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			
			int version = ByteUtil.readByteToInt(inputStream, 4);
			int objectParseVersion = ByteUtil.readByteToInt(inputStream, 4);
			int nameLength = ByteUtil.readByteToInt(inputStream, 4);
			String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
			
			int idLength = ByteUtil.readByteToInt(inputStream, 4);
			idLength = idLength < 4 ? 4 : idLength;
			String id = ByteUtil.readByteToStr(inputStream, idLength);
			
			int num = ByteUtil.readByteToInt(inputStream, 4);
			
			List<Map<String, Object>> effectList = new ArrayList<Map<String, Object>>();
			
			for(int i = 0; i < num; i ++) {
				Map<String, Object> effect = new HashMap<String, Object>();
				int keyLength = ByteUtil.readByteToInt(inputStream, 4);
				keyLength = keyLength < 4 ? 4 : keyLength;
				String key = ByteUtil.readByteToStr(inputStream, keyLength);
				
				effect.put("keyLength", keyLength);
				effect.put("key", key);
				effect.put("keyDesc", EffectObjTypeEnum.bm(key).name());
				IEffectObjHandler lfx2Handler = lfx2HandlerChain.get(key);
				if(lfx2Handler != null) {
					Map<String, Object> lfx2Data = lfx2Handler.handle(inputStream);
					effect.put("data", lfx2Data);
				}else {
					System.out.println("LayerEffectLfx2Handler[效果图层2].keyLength=" + keyLength);
					System.out.println("LayerEffectLfx2Handler[效果图层2].key=" + key + " not found");
				}
				effectList.add(effect);
			}
		
			System.out.println();
			Map<String, Object> map = new HashMap<>();
			map.put("version", version);
			map.put("objectParseVersion", objectParseVersion);
			map.put("nameLength", nameLength);
			map.put("name", name);
			map.put("idLength", idLength);
			map.put("id", id);
			map.put("num", num);
			map.put("effectList", effectList);
			return map;
		}
	}

}
