package top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 引用型对象 - 序号属性对象：
 * @author xiangtao
 * 
 */
public class EffectObjIndxHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int value = ByteUtil.readByteToInt(inputStream, 4);
		Map<String, Object> map = new HashMap<>();
		map.put("value", value);
		return map;
	}

}
