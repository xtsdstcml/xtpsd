package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface IEffectIrFXHandler {
	Map<String, Object> handle(InputStream inputStream) throws IOException ;
}
