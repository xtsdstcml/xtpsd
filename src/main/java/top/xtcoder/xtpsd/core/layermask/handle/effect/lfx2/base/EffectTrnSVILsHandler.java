package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * “不透明色标”为列表型对象
 */
public class EffectTrnSVILsHandler  implements IEffectObjHandler{
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Lctn.name(), new EffectLctnHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Mdpn.name(), new EffectMdpnHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			Map<String, Object> item = new HashMap<>();
			
			String objtype = ByteUtil.readByteToStr(inputStream, 4);
			item.put("type", objtype);
			
			String objName = ByteUtil.readByteToUnicodeStr(inputStream);
			item.put("name", objName);
			
			String objClassId = ByteUtil.readByteToClassId(inputStream);
			item.put("classId", objClassId);
			
			int propCount = ByteUtil.readByteToInt(inputStream, 4);
			item.put("count", propCount);
			
			List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
			for(int j = 0; j < propCount; j ++) {
				int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
				objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
				String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
				IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
				if(hc != null) {
					Map<String, Object> propertie = hc.handle(inputStream);
					propertie.put("id", objPropId);
					propertie.put("desc", EffectObjTypeEnum.bm(objPropId));
					properties.add(propertie);
				}else {
					System.out.println("EffectTrnSVILsHandler【" + objPropId + "】 not found");
				}
			}
			item.put("properties", properties);
			list.add(item);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //'type'为'VlLs'为列表型对象
		map.put("count", count);
		map.put("list", list);
		return map;
	}
}
