package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 结构：斜面和浮雕效果样式
 * @author xiangtao
 *
 */
public class EffectBvlSHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		Map<String, Object> map = super.handle(inputStream);
		String name = (String) map.get("enumName");
		if("OtrB".equals(name)) {
			map.put("label", "外斜面");
		}else if("InrB".equals(name)) {
			map.put("label", "内斜面");
		}else if("Embs".equals(name)) {
			map.put("label", "浮雕效果");
		}else if("PlEb".equals(name)) {
			map.put("label", "枕状浮雕");
		}else if("strokeEmboss".equals(name)) {
			map.put("label", "描边浮雕");
		}else {
			map.put("label", "未知");
		}
		return map;
	}
}
