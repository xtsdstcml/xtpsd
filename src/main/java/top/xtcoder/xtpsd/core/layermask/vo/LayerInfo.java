package top.xtcoder.xtpsd.core.layermask.vo;

import java.util.List;

public class LayerInfo {
	/**
	 * 数据长度
	 */
	private int layersLength;
	
	/**
	 * 图层数
	 */
	private int layerCount;
	
	/**
	 * 图层记录
	 */
	private List<LayerRecord> records;
	
	/**
	 * 通道图像数据。包含每个层的一个或多个图像数据记录（请参见通道图像数据以了解结构）。图层的顺序与图层信息（此表的前一行）中的顺序相同。
	 */
	private List<ChannelImageData> imageDatas;

	public int getLayersLength() {
		return layersLength;
	}

	public void setLayersLength(int layersLength) {
		this.layersLength = layersLength;
	}

	public int getLayerCount() {
		return layerCount;
	}

	public void setLayerCount(int layerCount) {
		this.layerCount = layerCount;
	}

	public List<LayerRecord> getRecords() {
		return records;
	}

	public void setRecords(List<LayerRecord> records) {
		this.records = records;
	}

	public List<ChannelImageData> getImageDatas() {
		return imageDatas;
	}

	public void setImageDatas(List<ChannelImageData> imageDatas) {
		this.imageDatas = imageDatas;
	}
}
