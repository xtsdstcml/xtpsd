package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * unicode对象
 * @author xiangtao
 *
 */
public class EffectUnicodeHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String name = ByteUtil.readByteToUnicodeStr(inputStream);
		Map<String, Object> map = new HashMap<>();
		map.put("name", name);
		return map;
	}

}
