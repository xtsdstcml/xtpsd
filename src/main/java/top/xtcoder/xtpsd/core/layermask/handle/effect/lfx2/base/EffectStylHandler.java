package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * “结构：位置”为枚举型
 * @author xiangtao
 *
 */
public class EffectStylHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		if("OutF".equals(String.valueOf(map.get("enumName")))) {
			map.put("label", "外部");
		}else if("InsF".equals(String.valueOf(map.get("enumName")))) {
			map.put("label", "内部");
		}else if("CtrF".equals(String.valueOf(map.get("enumName")))) {
			map.put("label", "居中");
		}
		return map;
	}
}
