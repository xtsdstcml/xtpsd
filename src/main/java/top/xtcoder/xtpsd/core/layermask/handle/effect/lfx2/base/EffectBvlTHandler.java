package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 结构：斜面方法
 * @author xiangtao
 *
 */
public class EffectBvlTHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		Map<String, Object> map = super.handle(inputStream);
		String name = (String) map.get("enumName");
		if("SfBL".equals(name)) {
			map.put("label", "浮雕柔和");
		}else if("PrBL".equals(name)) {
			map.put("label", "浮雕精确");
		}else if("Slmt".equals(name)) {
			map.put("label", "平滑");
		}else {
			map.put("label", "未知");
		}
		return map;
	}
}
