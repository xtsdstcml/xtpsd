package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeUntFHandler;

/**
 * 结构：斜面柔和度（软化）
 * @author xiangtao
 *
 */
public class EffectSftnHandler extends EffectTypeUntFHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		return map;
	}
}
