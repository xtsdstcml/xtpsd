package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;

/**
 * 品质：图层挖空投影标记”为布尔型
 * @author xiangtao
 *
 */
public class EffectLayerConcealsHandler extends EffectTypeBoolHandler{

}
