package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;

/**
 *  “自定义渐变”为枚举型
 * @author xiangtao
 *
 */
public class EffectGrdFHandler extends EffectTypeEnumHandler{
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		map.put("label", "自定义渐变");
		return map;
	}
}
