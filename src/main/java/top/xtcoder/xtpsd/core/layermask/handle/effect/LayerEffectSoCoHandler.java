package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectClrHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 纯色填充图层
 * @author xiangtao
 *
 */
public class LayerEffectSoCoHandler implements ILayerEffectHandler{
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			int version = ByteUtil.readByteToInt(inputStream, 4);
			map.put("version", version);
			
			String name = ByteUtil.readByteToUnicodeStr(inputStream);
			map.put("name", name);
			
			String id = ByteUtil.readByteToClassId(inputStream);
			map.put("id", id);
			
			int count = ByteUtil.readByteToInt(inputStream, 4);
			map.put("count", count);
			
			EffectClrHandler handler = new EffectClrHandler();
			String propertiesId = ByteUtil.readByteToClassId(inputStream);
			map.put("propertiesId", propertiesId);
			
			Map<String, Object> properties = handler.handle(inputStream);
			map.put("properties", properties);
			
			return map;
		}
	}
}
