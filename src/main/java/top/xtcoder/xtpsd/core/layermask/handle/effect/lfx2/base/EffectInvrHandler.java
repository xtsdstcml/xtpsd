package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;

/**
 * 结构：反相标记
 * @author xiangtao
 *
 */
public class EffectInvrHandler extends EffectTypeBoolHandler{
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		return map;
	}
}
