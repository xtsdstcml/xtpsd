package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 结构：方向
 * @author xiangtao
 *
 */
public class EffectBvlDHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		Map<String, Object> map = super.handle(inputStream);
		String name = (String) map.get("enumName");
		if("In".equals(name)) {
			map.put("label", "向上");
		}else if("Out".equals(name)) {
			map.put("label", "向下");
		}else {
			map.put("label", "未知");
		}
		return map;
	}
}
