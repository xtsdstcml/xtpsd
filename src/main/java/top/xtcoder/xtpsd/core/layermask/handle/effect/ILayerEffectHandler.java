package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.IOException;
import java.util.Map;

public interface ILayerEffectHandler {
	Map<String, Object> handle(byte[] effectData) throws IOException ;
}
