package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LayerEffectLclrHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("r", effectData[0]);
		map.put("g", effectData[1]);
		map.put("b", effectData[2]);
		map.put("a", effectData[3]);
		return map;
	}

}
