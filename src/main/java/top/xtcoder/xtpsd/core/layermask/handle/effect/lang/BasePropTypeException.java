package top.xtcoder.xtpsd.core.layermask.handle.effect.lang;

public class BasePropTypeException extends RuntimeException{
	public BasePropTypeException(String msg) {
		super(msg);
	}
}