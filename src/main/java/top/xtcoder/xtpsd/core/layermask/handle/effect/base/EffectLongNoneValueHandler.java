package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 整型对象:
 * @author xiangtao
 *
 */
public class EffectLongNoneValueHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = new HashMap<>();
		inputStream.mark(10);
		String nn = ByteUtil.readByteToStr(inputStream, 4);
		inputStream.reset();
		if(!"DfrC".equals(nn)) {
			double value = ByteUtil.readByteToDouble8(inputStream);
			map.put("value", value);
		}
		
		return map;
	}

}
