package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectShmdHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			int length = ByteUtil.readByteToInt(inputStream, 4);
			List<Map<String, Object>> shmds = new ArrayList<>();
			for(int i = 0; i < length; i++) {
				String signature = ByteUtil.readByteToStr(inputStream, 4);
				String key = ByteUtil.readByteToStr(inputStream, 4);
				int copyOnSheetDuplication = ByteUtil.readByteToInt(inputStream, 1);
				int padding = ByteUtil.readByteToInt(inputStream, 3);
				int dataLength = ByteUtil.readByteToInt(inputStream, 4);
				byte[] data = new byte[dataLength];
				inputStream.read(data);
				
				Map<String, Object> shmd = new HashMap<>();
				shmd.put("signature", signature);
				shmd.put("key", key);
				shmd.put("copyOnSheetDuplication", copyOnSheetDuplication);
				shmd.put("padding", padding);
				shmd.put("dataLength", dataLength);
//				shmd.put("data", data);
				shmds.add(shmd);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("length", length);
			map.put("shmds", shmds);
			return map;
		}
	}
}
