package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.dto;

public enum LayerEffectIrFXType {
	cmnS("cmnS", "效果图层公共状态"),
	dsdw("dsdw", "投影效果"),
	isdw("isdw", "内阴影效果"),
	oglw("oglw", "外发光效果"),
	iglw("iglw", "内发光效果"),
	bevl("bevl", "斜面和浮雕效果"),
	sofi("sofi", "颜色叠加效果"),
	;
	
	String value;
	String name;
	
	LayerEffectIrFXType(String value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public static LayerEffectIrFXType bm(String value) {
		LayerEffectIrFXType[] arr = LayerEffectIrFXType.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].value.equals(value)) {
				return arr[i];
			}
		}
		throw new RuntimeException("LayerEffectLfx2Type[" + value + " not found]");
	}
	
	@Override
	public String toString() {
		return value + ":" + name;
	}
}
