package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * Unicode文本型对象
 * @author xiangtao
 *
 */
public class EffectTextHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int textLength = ByteUtil.readByteToInt(inputStream, 4);
		String text = ByteUtil.readByteToUnicodeStr(inputStream, textLength * 2);
		Map<String, Object> map = new HashMap<>();
		map.put("textLength", textLength);
		map.put("text", text);
		return map;
	}

}
