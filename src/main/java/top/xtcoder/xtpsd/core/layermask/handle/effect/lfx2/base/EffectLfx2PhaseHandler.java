package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 纹理：方向
 * @author xiangtao
 *
 */
public class EffectLfx2PhaseHandler  implements IEffectObjHandler{

	private static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.Hrzn.name(), new EffectHrznDoubHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Vrtc.name(), new EffectHrznDoubHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int propIdLength = ByteUtil.readByteToInt(inputStream, 4);
			propIdLength = propIdLength < 4 ? 4 : propIdLength;
			String propId = ByteUtil.readByteToStr(inputStream, propIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(propId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", propId);
				propertie.put("desc", EffectObjTypeEnum.bm(propId));
				properties.add(propertie);
			}else {
				System.out.println("EffectLfx2GrFlHandler." + i + "【" + propId + "】 not found");
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("nameLength", nameLength);
		map.put("classId", classId);
		map.put("name", name);
		map.put("properties", properties);
		return map;
	}
}