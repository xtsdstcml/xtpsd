package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeUnicodeHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectBmpCHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectBmpCHandler.class);
	
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(LayerEffectPlLdType._name.n(), new EffectTypeUnicodeHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			String propType = ByteUtil.readByteToStr(inputStream, 4);
			
			IEffectObjHandler hc = lfx2HandlerChain.get(propType);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", propType);
				propertie.put("desc", LayerEffectPlLdType.bm(propType));
				properties.add(propertie);
			}else {
				String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
				log.log("id[%s]还未解析，后面一段字节的信息=%s", propType, info);
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
