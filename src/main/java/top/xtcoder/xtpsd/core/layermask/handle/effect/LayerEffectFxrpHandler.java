package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectFxrpHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			double x = ByteUtil.readByteToDouble8(inputStream);
			double y = ByteUtil.readByteToDouble8(inputStream);
			Map<String, Object> map = new HashMap<>();
			map.put("x", x);
			map.put("y", y);
			return map;
		}
		
	}

}
