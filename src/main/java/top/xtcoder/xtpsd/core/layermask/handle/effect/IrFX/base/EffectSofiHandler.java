package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 颜色叠加效果，效果签名为'sofi' (Photoshop 7.0)
 * @author xiangtao
 *
 */
public class EffectSofiHandler implements IEffectIrFXHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		//颜色叠加效果图层数据长度，总是为34
		int dataLength = ByteUtil.readByteToInt(inputStream, 4);
		
		//版本号，总是为2
		int version = ByteUtil.readByteToInt(inputStream, 4);
		
		//颜色：颜色叠加效果样式签名，总是为 '8BIM'
		String effectSign = ByteUtil.readByteToStr(inputStream, 4);
		
		//颜色：颜色叠加效果样式类型（见混合模式类型）
		String blendType = ByteUtil.readByteToStr(inputStream, 4);
		
		//颜色（2字节颜色空间类型 + 4*2字节颜色值）
		int colorType = ByteUtil.readByteToInt(inputStream, 2);
		int color1 = ByteUtil.readByteToInt(inputStream, 2);
		int color2 = ByteUtil.readByteToInt(inputStream, 2);
		int color3 = ByteUtil.readByteToInt(inputStream, 2);
		int color4 = ByteUtil.readByteToInt(inputStream, 2);
		
		//颜色：不透明度
		int opacity = ByteUtil.readByteToInt(inputStream, 1);
		//启用效果标记
		int enable = ByteUtil.readByteToInt(inputStream, 1);
		
		//天然颜色-颜色空间类型
		int naturalColorType = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor1 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor2 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor3 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor4 = ByteUtil.readByteToInt(inputStream, 2);
		
		Map<String, Object> map = new HashMap<>();
		map.put("dataLength", dataLength);
		map.put("version", version);
		map.put("effectSign", effectSign);
		map.put("blendType", blendType);
		
		map.put("colorType", colorType);
		map.put("color1", color1);
		map.put("color2", color2);
		map.put("color3", color3);
		map.put("color4", color4);
		
		map.put("opacity", opacity);
		map.put("enable", enable);
		
		map.put("naturalColorType", naturalColorType);
		map.put("naturalColor1", naturalColor1);
		map.put("naturalColor2", naturalColor2);
		map.put("naturalColor3", naturalColor3);
		map.put("naturalColor4", naturalColor4);
		return map;
	}
}
