package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectBevlHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectCmnSHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectDsdwHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectIglwHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectIsdwHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectOglwHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base.EffectSofiHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.dto.LayerEffectIrFXType;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectlrFXHandler implements ILayerEffectHandler{
	private static Map<String, IEffectIrFXHandler> IrFXHandlerChain = new HashMap<>();
	private static Log log = Log.getLog(LayerEffectlrFXHandler.class);
	static {
		IrFXHandlerChain.put(LayerEffectIrFXType.cmnS.name(), new EffectCmnSHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.dsdw.name(), new EffectDsdwHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.isdw.name(), new EffectIsdwHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.oglw.name(), new EffectOglwHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.iglw.name(), new EffectIglwHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.bevl.name(), new EffectBevlHandler());
		IrFXHandlerChain.put(LayerEffectIrFXType.sofi.name(), new EffectSofiHandler());
	}
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			int version = ByteUtil.readByteToInt(inputStream, 2);
			int effectNum = ByteUtil.readByteToInt(inputStream, 2);
			List<Map<String, Object>> effectList = new ArrayList<Map<String, Object>>();
			
			for(int i = 0; i < effectNum; i ++) {
				Map<String, Object> effect = new HashMap<String, Object>();
				String sign = ByteUtil.readByteToStr(inputStream, 4);
				String errectSign = ByteUtil.readByteToStr(inputStream, 4);
				effect.put("sign", sign);
				effect.put("errectSign", errectSign);
				effect.put("errectSignDesc", LayerEffectIrFXType.bm(errectSign).name());
				IEffectIrFXHandler lfx2Handler = IrFXHandlerChain.get(errectSign);
				if(lfx2Handler != null) {
					Map<String, Object> lfx2Data = lfx2Handler.handle(inputStream);
					effect.put("data", lfx2Data);
				}else {
					log.log("LayerEffectlrFXHandler[效果图层1].errectSign=" + errectSign + " not found");
				}
				effectList.add(effect);
			}
		
			Map<String, Object> map = new HashMap<>();
			map.put("version", version);
			map.put("effectNum", effectNum);
			map.put("effectList", effectList);
			return map;
		}
	}

}
