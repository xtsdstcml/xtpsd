package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 效果图层公共状态，效果签名为'cmnS'
 * @author xiangtao
 *
 */
public class EffectCmnSHandler implements IEffectIrFXHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int dataSize = ByteUtil.readByteToInt(inputStream, 4);
		int version = ByteUtil.readByteToInt(inputStream, 4);
		int sign = ByteUtil.readByteToInt(inputStream, 1);
		int baoliu = ByteUtil.readByteToInt(inputStream, dataSize - 5);
		Map<String, Object> map = new HashMap<>();
		map.put("dataSize", dataSize);
		map.put("version", version);
		map.put("sign", sign);
		map.put("baoliu", baoliu);
		return map;
	}
}
