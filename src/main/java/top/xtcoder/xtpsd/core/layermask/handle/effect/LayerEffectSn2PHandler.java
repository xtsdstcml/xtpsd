package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 使用对齐渲染
 * @author xiangtao
 *
 */
public class LayerEffectSn2PHandler implements ILayerEffectHandler{
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			int useAlignedRendering = ByteUtil.readByteToInt(inputStream, 4);
			map.put("useAlignedRendering", useAlignedRendering);
			return map;
		}
	}
}
