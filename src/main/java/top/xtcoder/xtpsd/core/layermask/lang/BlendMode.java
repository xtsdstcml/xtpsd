package top.xtcoder.xtpsd.core.layermask.lang;

public enum BlendMode {
	psd_blend_mode_normal("norm", "normal[正常]"),		
	psd_blend_mode_dissolve("diss", "dissolve[溶解]"),	
	psd_blend_mode_darken("dark", "darken[变暗]"),		
	psd_blend_mode_multiply("mul", "multiply[正片叠底]"),
	psd_blend_mode_color_burn("idiv", "color burn[颜色加深]"),	
	psd_blend_mode_linear_burn("lbrn", "linear burn[线性加深]"),	
	psd_blend_mode_lighten("lite", "lighten[变亮]"),		
	psd_blend_mode_screen("scrn", "screen[滤色]"),		
	psd_blend_mode_color_dodge("div", "color dodge[颜色减淡]"),
	psd_blend_mode_linear_dodge("lddg", "linear dodge[线性减淡[添加]]"),
	psd_blend_mode_overlay("over", "overlay[叠加]"),		
	psd_blend_mode_soft_light("sLit", "soft light[柔光]"),	
	psd_blend_mode_hard_light("hLit", "hard light[强光]"),	
	psd_blend_mode_vivid_light("vLit", "vivid light[亮光]"),	
	psd_blend_mode_linear_light("lLit", "linear light[线性光]"),
	psd_blend_mode_pin_light("pLit", "pin light[点光]"),	
	psd_blend_mode_hard_mix("hMix", "hard mix[实色混合]"),	
	psd_blend_mode_difference("diff", "difference[差值]"),
	psd_blend_mode_exclusion("smud", "exclusion[排除]"),	
	psd_blend_mode_hue("hue", "hue[色相]"),
	psd_blend_mode_saturation("sat", "saturation[饱和度]"),
	psd_blend_mode_color("colr", "color[颜色]"),		
	psd_blend_mode_luminosity("lum", "luminosity[明度]"),		// ' ' = 
	psd_blend_mode_pass_through("pass", "pass[通过]");
	
	String value;
	String name;
	
	BlendMode(String value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public static BlendMode bm(String value) {
		BlendMode[] arr = BlendMode.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].value.equals(value)) {
				return arr[i];
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return value + ":" + name;
	}
}
