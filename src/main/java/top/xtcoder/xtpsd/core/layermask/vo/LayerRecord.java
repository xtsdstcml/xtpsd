package top.xtcoder.xtpsd.core.layermask.vo;

import java.util.List;

public class LayerRecord {
	/**
	 * 图层范围
	 */
	private LayerRecordCoordinate coordinate;
	
	/**
	 * 图层通道的头信息
	 */
	private ChannelInformation channelInformation;
	
	/**
	 * 混合模式
	 */
	private BlendModeInfo blendMode;
	
	/**
	 * 不透明度， 0为透明，255为不透明
	 */
	private int opacity;
	
	/**
	 * 剪贴蒙版标记，0 为底板, 1为贴图
	 */
	private int clipping;
	
	/**
	 * 图层设置标记
	 */
	private LayerSetFlagInfo flag;
	
	/**
	 * 图层设置保留标记
	 */
	private int filler;
	
	/**
	 * 扩展层长度：(图层附加效果层数据长度+图层蒙版信息数据+图层混合颜色带信息数据+图层名称）
	 */
	private int extraDataLength;
	
	/**
	 * 图层蒙版信息数据
	 */
	private LayerMaskData maskData;
	
	/**
	 * 图层混合颜色带信息数据
	 */
	private LayerBlendingRange blendingRange;
	
	/**
	 * 图层名称
	 */
	private LayerNameInfo nameInfo;
	
	private List<LayerEffect> effects;
	
	public LayerRecordCoordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(LayerRecordCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	public ChannelInformation getChannelInformation() {
		return channelInformation;
	}

	public void setChannelInformation(ChannelInformation channelInformation) {
		this.channelInformation = channelInformation;
	}

	public BlendModeInfo getBlendMode() {
		return blendMode;
	}

	public void setBlendMode(BlendModeInfo blendMode) {
		this.blendMode = blendMode;
	}

	public int getOpacity() {
		return opacity;
	}

	public void setOpacity(int opacity) {
		this.opacity = opacity;
	}

	public int getClipping() {
		return clipping;
	}

	public void setClipping(int clipping) {
		this.clipping = clipping;
	}

	public LayerSetFlagInfo getFlag() {
		return flag;
	}

	public void setFlag(LayerSetFlagInfo flag) {
		this.flag = flag;
	}

	public int getFiller() {
		return filler;
	}

	public void setFiller(int filler) {
		this.filler = filler;
	}

	public int getExtraDataLength() {
		return extraDataLength;
	}

	public void setExtraDataLength(int extraDataLength) {
		this.extraDataLength = extraDataLength;
	}

	public LayerMaskData getMaskData() {
		return maskData;
	}

	public void setMaskData(LayerMaskData maskData) {
		this.maskData = maskData;
	}

	public LayerBlendingRange getBlendingRange() {
		return blendingRange;
	}

	public void setBlendingRange(LayerBlendingRange blendingRange) {
		this.blendingRange = blendingRange;
	}

	public LayerNameInfo getNameInfo() {
		return nameInfo;
	}

	public void setNameInfo(LayerNameInfo nameInfo) {
		this.nameInfo = nameInfo;
	}

	public List<LayerEffect> getEffects() {
		return effects;
	}

	public void setEffects(List<LayerEffect> effects) {
		this.effects = effects;
	}
}
