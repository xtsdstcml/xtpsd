package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectClrHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 矢量笔划内容
 * @author xiangtao
 *
 */
public class LayerEffectVscgHandler implements ILayerEffectHandler{

	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		
	}
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			//int length = ByteUtil.readByteToInt(inputStream, 4);
			Map<String, Object> map = new HashMap<>();
			
			String key = ByteUtil.readByteToStr(inputStream, 4);
			int version = ByteUtil.readByteToInt(inputStream, 4);
			map.put("key", key);
			map.put("version", version);
			
			String name = ByteUtil.readByteToUnicodeStr(inputStream);
			map.put("name", name);
			
			String classID = ByteUtil.readByteToClassId(inputStream);
			map.put("classID", classID);
			
			int number = ByteUtil.readByteToInt(inputStream, 4);
			map.put("number", number);
			
			List<Map<String, Object>> items = new ArrayList();
			for(int i = 0; i < number; i ++) {
				String id = ByteUtil.readByteToClassId(inputStream);
				IEffectObjHandler handler = handlerChain.get(id);
				if(handler != null) {
					Map<String, Object> res = handler.handle(inputStream);
					res.put("id", id);
					items.add(res);
				}else {
					System.out.println("LayerEffectVscgHandler[" + id + "] not found");
				}
			}
			map.put("items", items);
			return map;
		}
	}
}
