package top.xtcoder.xtpsd.core.layermask.vo;

public class LayerRecordCoordinate extends Location{

	public LayerRecordCoordinate() {
		super();
	}
	
	public LayerRecordCoordinate(int top, int right, int bottom, int left) {
		super(top, right, bottom, left);
	}
	
}
