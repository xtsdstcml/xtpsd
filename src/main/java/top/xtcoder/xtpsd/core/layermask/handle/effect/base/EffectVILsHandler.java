package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lang.EffectObjHandleChain;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;


public class EffectVILsHandler implements IEffectObjHandler{
private static final Log log = Log.getLog(EffectVILsHandler.class);
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			String type = ByteUtil.readByteToStr(inputStream, 4);
			IEffectObjHandler hc = EffectObjHandleChain.get(type);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("type", type);
				properties.add(propertie);
			}else {
				ByteUtil.viewStreamByteInfo(log, "VILs", type, inputStream);
				while(inputStream.read() != -1);
				break;
			}
		}

		Map<String, Object> map = new HashMap<>();
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
