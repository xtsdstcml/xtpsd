package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 布尔型对象:
 * @author xiangtao
 *
 */
public class EffectBoolHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int value = ByteUtil.readByteToInt(inputStream, 1);
		Map<String, Object> map = new HashMap<>();
		map.put("value", value);
		return map;
	}

}
