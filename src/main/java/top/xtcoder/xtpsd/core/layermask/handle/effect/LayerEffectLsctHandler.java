package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectLsctHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		Map<String, Object> map = new HashMap<>();
		try(InputStream inputStream = new ByteArrayInputStream(effectData);){
			int type = ByteUtil.readByteToInt(inputStream, 4);
			String sign = ByteUtil.readByteToStr(inputStream, 4);
			String layerSplitMode = ByteUtil.readByteToStr(inputStream, 4);
			
			map.put("type", type);
			map.put("sign", sign);
			map.put("layerSplitMode", layerSplitMode);
		}
		return map;
	}

}
