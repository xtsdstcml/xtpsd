package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeUntFHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 渐变色信息
 * @author xiangtao
 *
 */
public class EffectGradHandler implements IEffectObjHandler{

	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.bool.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum._long.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.doub.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.TEXT.name(), new EffectTypeTextHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.UntF.name(), new EffectTypeUntFHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum._enum.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.alis.name(), new EffectLfx2AlisHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.type.name(), new EffectLfx2TypeHandler());
		
		lfx2HandlerChain.put(EffectObjTypeEnum.Rd.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Grn.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Bl.name(), new EffectTypeDoubleHandler());
		
		lfx2HandlerChain.put(EffectObjTypeEnum.Nm.name(), new EffectNmHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.GrdF.name(), new EffectGrdFHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Intr.name(), new EffectIntrHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Clrs.name(), new EffectClrsHandler());
		
		lfx2HandlerChain.put(EffectObjTypeEnum.Trns.name(), new EffectTrnSVILsHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(objPropId.trim());
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", EffectObjTypeEnum.bm(objPropId));
				properties.add(propertie);
			}else {
				System.out.println("EffectGradHandler【" + objPropId + "】not found");
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //'type'为类，'GlbC'为全局类
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
