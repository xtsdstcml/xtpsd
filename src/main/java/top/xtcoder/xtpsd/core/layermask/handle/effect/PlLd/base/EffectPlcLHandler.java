package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.Point;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class EffectPlcLHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		int version = ByteUtil.readByteToInt(inputStream, 4);
		map.put("version", version);
		
		String uniqueId = ByteUtil.readByteToPascalStr(inputStream);
		map.put("uniqueId", uniqueId);
		
		int pageNumber = ByteUtil.readByteToInt(inputStream, 4);
		map.put("pageNumber", pageNumber);
		
		int totalPage = ByteUtil.readByteToInt(inputStream, 4);
		map.put("totalPage", totalPage);
		
		int anitAliasPolicy = ByteUtil.readByteToInt(inputStream, 4);
		map.put("anitAliasPolicy", anitAliasPolicy);
		
		int placeLayerType = ByteUtil.readByteToInt(inputStream, 4);
		map.put("placeLayerType", placeLayerType);
		if(placeLayerType == 0) {
			map.put("placeLayerName", "未知");
		}else if(placeLayerType == 1) {
			map.put("placeLayerName", "矢量");
		}else if(placeLayerType == 2) {
			map.put("placeLayerName", "光栅");
		}else if(placeLayerType == 3) {
			map.put("placeLayerName", "图像堆栈");
		}
		List<Point> transformationPoints = new ArrayList<>();
		for(int i = 0; i < 4; i ++) {
			double x = ByteUtil.readByteToDouble8(inputStream);
			double y = ByteUtil.readByteToDouble8(inputStream);
			Point p = new Point(x, y);
			transformationPoints.add(p);
		}
		map.put("transformationPoints", transformationPoints);
		
		int warpVersion = ByteUtil.readByteToInt(inputStream, 4);
		map.put("warpVersion", warpVersion);
		
		int wrapDescriptorVersion = ByteUtil.readByteToInt(inputStream, 4);
		map.put("wrapDescriptorVersion", wrapDescriptorVersion);
		return map;
	}
}
