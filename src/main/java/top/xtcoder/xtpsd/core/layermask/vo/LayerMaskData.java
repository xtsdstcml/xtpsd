package top.xtcoder.xtpsd.core.layermask.vo;

public class LayerMaskData {
	/**
	 * 数据长度 图层蒙版信息数据长度，可能的数值有0、20、36，如果为零，则表示没有数据。
	 * 
	 */
	private int dataSize;
	
	/**
	 * 位置信息
	 */
	private LayerMaskCoordinate coordinate;
	
	/**
	 * 蒙版图层的默认颜色（0或255）
	 */
	private int defaultColor;
	
	private LayerMaskFlagInfo flagInfo;
	
	private byte[] maskParameters;
	
	private int totalDataSize;
	
	public int getDataSize() {
		return dataSize;
	}

	public void setDataSize(int dataSize) {
		this.dataSize = dataSize;
	}

	public LayerMaskCoordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(LayerMaskCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	public int getDefaultColor() {
		return defaultColor;
	}

	public void setDefaultColor(int defaultColor) {
		this.defaultColor = defaultColor;
	}

	public LayerMaskFlagInfo getFlagInfo() {
		return flagInfo;
	}

	public void setFlagInfo(LayerMaskFlagInfo flagInfo) {
		this.flagInfo = flagInfo;
	}

	public byte[] getMaskParameters() {
		return maskParameters;
	}

	public void setMaskParameters(byte[] maskParameters) {
		this.maskParameters = maskParameters;
	}

	public int getTotalDataSize() {
		return totalDataSize;
	}

	public void setTotalDataSize(int totalDataSize) {
		this.totalDataSize = totalDataSize;
	}
}
