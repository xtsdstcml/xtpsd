package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectClrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectEnabHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectOpctHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class EffectLfx2SoFiHandler implements IEffectObjHandler {
	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		handlerChain.put(EffectObjTypeEnum.Md.name(), new EffectMdHandler());
		handlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		handlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> data = new HashMap<String, Object>();
		
		String type = ByteUtil.readByteToStr(inputStream, 4);
		data.put("type", type);
		
		String name = ByteUtil.readByteToUnicodeStr(inputStream);
		data.put("name", name);
		
		String classId = ByteUtil.readByteToClassId(inputStream);
		data.put("classId", classId);
		
		int count = ByteUtil.readByteToInt(inputStream, 4); //这个应该是有多少个属性字段数量
		data.put("count", count);
		
		String enabId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> enabInfo = handlerChain.get(enabId).handle(inputStream);
		enabInfo.put("id", enabId);
		enabInfo.put("desc", EffectObjTypeEnum.bm(enabId));
		data.put("enabInfo", enabInfo);
		
		String mdId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> mdInfo = handlerChain.get(mdId).handle(inputStream);
		mdInfo.put("id", mdId);
		mdInfo.put("desc", EffectObjTypeEnum.bm(mdId));
		data.put("colorOverlayStyle", mdInfo);
		
		String opctId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> opctInfo = handlerChain.get(opctId).handle(inputStream);
		opctInfo.put("id", opctId);
		opctInfo.put("desc", EffectObjTypeEnum.bm(opctId));
		data.put("opacity", mdInfo);
		
		String clrId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> clrInfo = handlerChain.get(clrId).handle(inputStream);
		clrInfo.put("id", clrId);
		clrInfo.put("desc", EffectObjTypeEnum.bm(clrId));
		data.put("color", clrInfo);
		
		return data;
	}

}
