package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectFilterIdHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectFilterIdHandler.class);
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 12);
		String str = ByteUtil.readByteToStr(inputStream, 8);
		int dd = ByteUtil.readByteToInt(inputStream, 2);
		System.out.println("FilterId.type=" + type);
		System.out.println("FilterId.str=" + str);
		System.out.println("FilterId.dd=" + dd);
		Map<String, Object> map = new HashMap<>();
		return map;
	}
}
