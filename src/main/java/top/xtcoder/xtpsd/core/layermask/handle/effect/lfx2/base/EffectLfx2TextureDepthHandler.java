package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeUntFHandler;

/**
 * 纹理：深度”为单位值型
 * @author xiangtao
 *
 */
public class EffectLfx2TextureDepthHandler extends EffectTypeUntFHandler{
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		return map;
	}
}
