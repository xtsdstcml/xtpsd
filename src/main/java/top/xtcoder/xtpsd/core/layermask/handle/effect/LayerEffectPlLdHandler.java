package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.EffectPlcLHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.EffectSoLdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 放置图层
 * @author xiangtao
 *
 */
public class LayerEffectPlLdHandler implements ILayerEffectHandler{
	private static final Log log = Log.getLog(LayerEffectPlLdHandler.class);
	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(LayerEffectPlLdType.plcL.name(), new EffectPlcLHandler());
		handlerChain.put(LayerEffectPlLdType.soLD.name(), new EffectSoLdHandler());
	}
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			String type = ByteUtil.readByteToStr(inputStream, 4);
			IEffectObjHandler h = handlerChain.get(type);
			if(h != null) {
				Map<String, Object> properties = h.handle(inputStream);
				map.put("properties", properties);
			}else {
				ByteUtil.viewStreamByteInfo(log, "PILd", type, inputStream);
			}
			return map;
		}
	}
}
