package top.xtcoder.xtpsd.core.layermask.handle.effect.lang;

import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectObjcHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTdtaHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectUntFHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectVILsHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;

public class EffectObjHandleChain {
	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	static {
		handlerChain.put(BasePropType.TEXT.key(), new EffectTextHandler());
		handlerChain.put(BasePropType._long.key(), new EffectLongHandler());
		handlerChain.put(BasePropType.doub.key(), new EffectDoubleHandler());
		handlerChain.put(BasePropType.bool.key(), new EffectBoolHandler());
		handlerChain.put(BasePropType._enum.key(), new EffectEnumHandler());
		handlerChain.put(BasePropType.UntF.key(), new EffectUntFHandler());
		handlerChain.put(BasePropType.obj.key(), new EffectObjHandler());
		handlerChain.put(BasePropType.Objc.key(), new EffectObjcHandler());
		handlerChain.put(BasePropType.VlLs.key(), new EffectVILsHandler());
		handlerChain.put(BasePropType.tdta.key(), new EffectTdtaHandler());
	}
	
	public static IEffectObjHandler get(String key) {
		return handlerChain.get(key);
	}
}
