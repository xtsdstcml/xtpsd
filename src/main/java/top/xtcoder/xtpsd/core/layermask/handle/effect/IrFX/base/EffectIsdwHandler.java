package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 内阴影效果，效果签名为'isdw'
 * @author xiangtao
 *
 */
public class EffectIsdwHandler implements IEffectIrFXHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int dataLength = ByteUtil.readByteToInt(inputStream, 4);
		int version = ByteUtil.readByteToInt(inputStream, 4);
		
		//阴影大小
		int shadowSize = ByteUtil.readByteToInt(inputStream, 2);
		
		//图层蒙版扩展（0-100）
		int layerMaskExtension = ByteUtil.readByteToInt(inputStream, 4);
		
		//光源角度
		int lightAngle = ByteUtil.readByteToInt(inputStream, 4);
		
		//阴影位移距离
		int shadowDisplacementDistance = ByteUtil.readByteToInt(inputStream, 4);
		
		int baoliu = ByteUtil.readByteToInt(inputStream, 2);
		
		//阴影颜色-颜色空间类型
		int shadowColorType = ByteUtil.readByteToInt(inputStream, 2);
		int shadowColor1 = ByteUtil.readByteToInt(inputStream, 2);
		int shadowColor2 = ByteUtil.readByteToInt(inputStream, 2);
		int shadowColor3 = ByteUtil.readByteToInt(inputStream, 2);
		int shadowColor4 = ByteUtil.readByteToInt(inputStream, 2);
		
		String sign = ByteUtil.readByteToStr(inputStream, 4);
		String blendType = ByteUtil.readByteToStr(inputStream, 4);
		int enable = ByteUtil.readByteToInt(inputStream, 1);
		int useGlobalLight = ByteUtil.readByteToInt(inputStream, 1);
		int opacity = ByteUtil.readByteToInt(inputStream, 1);
		
		//天然颜色-颜色空间类型
		int naturalColorType = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor1 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor2 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor3 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor4 = ByteUtil.readByteToInt(inputStream, 2);
		
		Map<String, Object> map = new HashMap<>();
		map.put("dataLength", dataLength);
		map.put("version", version);
		map.put("shadowSize", shadowSize);
		map.put("layerMaskExtension", layerMaskExtension);
		map.put("lightAngle", lightAngle);
		map.put("shadowDisplacementDistance", shadowDisplacementDistance);
		map.put("baoliu", baoliu);
		
		map.put("shadowColorType", shadowColorType);
		map.put("shadowColor1", shadowColor1);
		map.put("shadowColor2", shadowColor2);
		map.put("shadowColor3", shadowColor3);
		map.put("shadowColor4", shadowColor4);
		
		map.put("sign", sign);
		map.put("blendType", blendType);
		map.put("enable", enable);
		map.put("useGlobalLight", useGlobalLight);
		map.put("opacity", opacity);
		
		map.put("naturalColorType", naturalColorType);
		map.put("naturalColor1", naturalColor1);
		map.put("naturalColor2", naturalColor2);
		map.put("naturalColor3", naturalColor3);
		map.put("naturalColor4", naturalColor4);
		
		return map;
	}
}
