package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 枚举型对象
 * @author xiangtao
 *
 */
public class EffectEnumHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int enumIdLength = ByteUtil.readByteToInt(inputStream, 4);
		enumIdLength = enumIdLength < 4 ? 4 : enumIdLength;
		String enumId = ByteUtil.readByteToStr(inputStream, enumIdLength);
		
		int enumLength = ByteUtil.readByteToInt(inputStream, 4);
		enumLength = enumLength < 4 ? 4 : enumLength;
		String enumName = ByteUtil.readByteToStr(inputStream, enumLength);
		
		Map<String, Object> map = new HashMap<>();
		map.put("enumIdLength", enumIdLength);
		map.put("enumId", enumId);
		map.put("enumLength", enumLength);
		map.put("enumName", enumName);
		return map;
	}
}
