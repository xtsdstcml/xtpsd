package top.xtcoder.xtpsd.core.layermask.vo;

public class LayerMaskCoordinate extends Location{

	public LayerMaskCoordinate(int top, int right, int bottom, int left) {
		super(top, right, bottom, left);
	}

}
