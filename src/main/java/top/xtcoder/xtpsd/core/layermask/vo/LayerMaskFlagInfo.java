package top.xtcoder.xtpsd.core.layermask.vo;

public class LayerMaskFlagInfo {
	private String flagStr;

	/**
	 * 相对于图层的位置标记
	 */
	private int  position;
	
	/**
	 * 关闭图层蒙版标记(过时)
	 */
	private int disabled;
	
	/**
	 * 混合时反转图层蒙版标记
	 */
	private int blending;
	
	/**
	 * 表示用户掩码实际上来自于渲染其他数据
	 */
	private int maskRendering;
	
	/**
	 * 指示用户和/或矢量掩码应用了参数
	 */
	private int maskParameters;

	public String getFlagStr() {
		return flagStr;
	}

	public void setFlagStr(String flagStr) {
		this.flagStr = flagStr;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getDisabled() {
		return disabled;
	}

	public void setDisabled(int disabled) {
		this.disabled = disabled;
	}

	public int getBlending() {
		return blending;
	}

	public void setBlending(int blending) {
		this.blending = blending;
	}

	public int getMaskRendering() {
		return maskRendering;
	}

	public void setMaskRendering(int maskRendering) {
		this.maskRendering = maskRendering;
	}

	public int getMaskParameters() {
		return maskParameters;
	}

	public void setMaskParameters(int maskParameters) {
		this.maskParameters = maskParameters;
	}
}
