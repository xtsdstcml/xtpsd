package top.xtcoder.xtpsd.core.layermask.vo;

/**
 * 图层设置标记，每一位为一个标记，由右向左表示
 * @author xiangtao
 *
 */
public class LayerSetFlagInfo {
	
	private String flagStr;

	/**
	 * 透明度保护标记
	 */
	private int transparencyProtected;
	
	/**
	 * 显示图层标记
	 */
	private int visible;
	
	/**
	 * 停用图层标记
	 */
	private int obsolete;
	
	/**
	 * 适用于Photoshop 5.0及更高版本，告诉第4位是否有有用信息
	 */
	private int useful4;
	
	/**
	 * 与文档外观无关的像素数据
	 */
	private int documentPixData;
	
	public String getFlagStr() {
		return flagStr;
	}

	public void setFlagStr(String flagStr) {
		this.flagStr = flagStr;
	}

	public int getTransparencyProtected() {
		return transparencyProtected;
	}

	public void setTransparencyProtected(int transparencyProtected) {
		this.transparencyProtected = transparencyProtected;
	}

	public int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public int getObsolete() {
		return obsolete;
	}

	public void setObsolete(int obsolete) {
		this.obsolete = obsolete;
	}

	public int getUseful4() {
		return useful4;
	}

	public void setUseful4(int useful4) {
		this.useful4 = useful4;
	}

	public int getDocumentPixData() {
		return documentPixData;
	}

	public void setDocumentPixData(int documentPixData) {
		this.documentPixData = documentPixData;
	}
	
}
