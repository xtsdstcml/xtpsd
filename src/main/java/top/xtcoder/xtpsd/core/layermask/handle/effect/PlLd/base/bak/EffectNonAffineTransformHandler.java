package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLctnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMdpnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectOpctHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;


public class EffectNonAffineTransformHandler  implements IEffectObjHandler{
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		EffectTypeDoubleHandler doubleHandler = new EffectTypeDoubleHandler();
		
		List<Map<String, Object>> values = new ArrayList();
		for(int i = 0; i < count; i ++) {
			Map<String, Object> val = doubleHandler.handle(inputStream);
			values.add(val);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("count", count);
		map.put("values", values);
		return map;
	}
}
