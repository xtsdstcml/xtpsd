package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto;

import top.xtcoder.xtpsd.exception.LayerEffectLfx2TypeException;

public enum LayerEffectPlLdType {
	plcL("plcL", "放置层"),
	soLD("soLD", "放置层"),
	
	Idnt("Idnt", "Idnt"),
	placed("placed", "placed"),
	PgNm("PgNm", "PgNm"),
	totalPages("totalPages", "totalPages"),
	frameStep("frameStep", "frameStep"),
	frameCount("frameCount", "frameCount"),
	Annt("Annt", "Annt"),
	Type("Type", "Type"),
	Trnf("Trnf", "Trnf"),
	numerator("numerator", "Trnf"),
	denominator("denominator", "denominator"),
	duration("duration", "duration"),
	warp("warp", "warp"),
	warpStyle("warpStyle", "warpStyle"),
	warpValue("warpValue", "warpStyle"),
	warpPerspective("warpPerspective", "warpPerspective"),
	warpPerspectiveOther("warpPerspectiveOther", "warpPerspectiveOther"),
	warpRotate("warpRotate", "warpRotate"),
	nonAffineTransform("nonAffineTransform", "nonAffineTransform"),
	
	bounds("bounds", "bounds"),
	Top("Top", "Top"),
	Left("Left", "Left"),
	Btom("Btom", "Btom"),
	Rght("Rght", "Rght"),
	
	uOrder("uOrder", "uOrder"),
	vOrder("vOrder", "vOrder"),
	
	Sz("Sz", "Sz"),
	Wdth("Wdth", "Wdth"),
	Hght("Hght", "Hght"),
	Rslt("Rslt", "Rslt"),
	Angl("Angl", "Angl"),
	Dstn("Dstn", "Dstn"),
	WndM("WndM", "WndM"),
	Drct("Drct", "Drct"),
	GEfk("GEfk", "GEfk"),
	FbrL("FbrL", "FbrL"),
	
	filterFX("filterFX", "filterFX"),
	enab("enab", "enab"),
	validAtPosition("validAtPosition", "validAtPosition"),
	filterMaskEnable("filterMaskEnable", "filterMaskEnable"),
	filterMaskLinked("filterMaskLinked", "filterMaskLinked"),
	filterMaskExtendWithWhite("filterMaskExtendWithWhite", "filterMaskExtendWithWhite"),
	filterFXList("filterFXList", "filterFXList"),
	
	Nm("Nm", "Nm"),
	blendOptions("blendOptions", "blendOptions"),
	hasoptions("hasoptions", "hasoptions"),
	FrgC("FrgC", "FrgC"),
	BckC("BckC", "BckC"),
	Fltr("Fltr", "Fltr"),
	
	Opct("Opct", "Opct"),
	Md("Md", "Md"),
	
	H("H", "H"),
	Strt("Strt", "Strt"),
	Brgh("Brgh", "Brgh"),
	
	Rd("Rd", "Rd"),
	Grn("Grn", "Grn"),
	Bl("Bl", "Bl"),
	
	FlRs("FlRs", "FlRs"),
	IntE("IntE", "IntE"),
	IntC("IntC", "IntC"),
	Cntr("Cntr", "Cntr"),
	Lvl("Lvl", "Lvl"),
	Edg("Edg", "Edg"),
	filterID("filterID", "filterID"),
	k3DLights("k3DLights", "k3DLights"),
	hots("hots", "hots"),
	FlOf("FlOf", "FlOf"),
	shdw("shdw", "shdw"),
	attn("attn", "attn"),
	attt("attt", "attt"),
	atta("atta", "atta"),
	attb("attb", "attb"),
	attc("attc", "attc"),
	orad("orad", "orad"),
	irad("irad", "irad"),
	mult("mult", "mult"),
	ison("ison", "ison"),
	ssml("ssml", "ssml"),
	afon("afon", "afon"),
	afpw("afpw", "afpw"),
	key3DPosition("key3DPosition", "key3DPosition"),
	key3DMatrix("key3DMatrix", "key3DMatrix"),
	key3DMatrixData("key3DMatrixData", "key3DMatrixData"),
	X("X", "X"),
	Y("Y", "Y"),
	Z("Z", "Z"),
	tarx("tarx", "tarx"),
	tary("tary", "tary"),
	tarz("tarz", "tarz"),
	
	key3DXPos("key3DXPos", "key3DXPos"),
	key3DYPos("key3DYPos", "key3DYPos"),
	key3DZPos("key3DZPos", "key3DZPos"),
	key3DXAngle("key3DXAngle", "key3DXAngle"),
	key3DYAngle("key3DYAngle", "key3DYAngle"),
	key3DZAngle("key3DZAngle", "key3DZAngle"),
	
	key3DCurrentCameraPosition("key3DCurrentCameraPosition", "key3DCurrentCameraPosition"),
	
	Glos("Glos", "Glos"),
	Mtrl("Mtrl", "Mtrl"),
	Exps("Exps", "Exps"),
	AmbB("AmbB", "AmbB"),
	AmbC("AmbC", "AmbC"),
	BmpA("BmpA", "BmpA"),
	BmpC("BmpC", "BmpC"),
	Amnt("Amnt", "Amnt"),
	TlNm("TlNm", "TlNm"),
	TlOf("TlOf", "TlOf"),
	FlCl("FlCl", "FlCl"),
	ExtS("ExtS", "ExtS"),
	ExtD("ExtD", "ExtD"),
	ExtF("ExtF", "ExtF"),
	ExtM("ExtM", "ExtM"),
	ExtT("ExtT", "ExtT"),
	ExtR("ExtR", "ExtR"),
	
	_name("name", "name"),
	;
	
	String value;
	String name;
	
	LayerEffectPlLdType(String value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public static String bm(String value) {
		LayerEffectPlLdType[] arr = LayerEffectPlLdType.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].value.equals(value)) {
				return arr[i].toString();
			}
		}
		return ("LayerEffectLfx2Type[" + value + " not found]");
	}
	
	public String n() {
		return name;
	}
	
	public String v() {
		return value;
	}
	
	@Override
	public String toString() {
		return value + ":" + name;
	}
}
