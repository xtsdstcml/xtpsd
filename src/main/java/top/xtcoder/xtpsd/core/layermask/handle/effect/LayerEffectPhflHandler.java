package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectPhflHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			int version = ByteUtil.readByteToInt(inputStream, 2);
			map.put("version", version);
			if(version == 3) {
				int xcolor = ByteUtil.readByteToInt(inputStream, 4);
				map.put("xcolor", xcolor);
				
				int ycolor = ByteUtil.readByteToInt(inputStream, 4);
				map.put("ycolor", ycolor);
				
				int zcolor = ByteUtil.readByteToInt(inputStream, 4);
				map.put("zcolor", zcolor);
			} else if(version == 2){
				int colorType = ByteUtil.readByteToInt(inputStream, 2);
				map.put("colorType", colorType);
				
				int color1 = ByteUtil.readByteToInt(inputStream, 2);
				map.put("color1", color1);
				
				int color2 = ByteUtil.readByteToInt(inputStream, 2);
				map.put("color2", color2);
				
				int color3 = ByteUtil.readByteToInt(inputStream, 2);
				map.put("color3", color3);
				
				int color4 = ByteUtil.readByteToInt(inputStream, 2);
				map.put("color4", color4);
			}
			
			int density = ByteUtil.readByteToInt(inputStream, 4);
			map.put("density", density);
			
			int maintainBrightness = ByteUtil.readByteToInt(inputStream, 1);
			map.put("maintainBrightness", maintainBrightness);
			
			return map;
		}
	}

}
