package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectLyidHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		int layerID = ByteUtil.bytesToInt(effectData);
		Map<String, Object> map = new HashMap<>();
		map.put("layerID", layerID);
		return map;
	}

}
