package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectAlgnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectAntAHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBlurHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBvlDHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBvlSHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBvlTHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectEnabHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectHglCHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectHglMHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectHglOHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectInprHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectInvTHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLaldHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2AntialiasGlossHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2LaglHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2PhaseHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2TextureDepthHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2UseShapeHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectLfx2UseTextureHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMpgSHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectPtrnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSclHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSdwCHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSdwMHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSdwOHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSftnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectSrgRHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectTrnSObjcHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectUglgHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 斜面和浮雕效果图层
 * @author xiangtao
 *
 */
public class EffectLfx2EbblHandler implements IEffectObjHandler {
	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(EffectObjTypeEnum.antialiasGloss.name(), new EffectLfx2AntialiasGlossHandler());
		handlerChain.put(EffectObjTypeEnum.useShape.name(), new EffectLfx2UseShapeHandler());
		handlerChain.put(EffectObjTypeEnum.useTexture.name(), new EffectLfx2UseTextureHandler());
		handlerChain.put(EffectObjTypeEnum.textureDepth.name(), new EffectLfx2TextureDepthHandler());
		handlerChain.put(EffectObjTypeEnum.phase.name(), new EffectLfx2PhaseHandler());
		handlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		handlerChain.put(EffectObjTypeEnum.hglM.name(), new EffectHglMHandler());
		handlerChain.put(EffectObjTypeEnum.hglO.name(), new EffectHglOHandler());
		handlerChain.put(EffectObjTypeEnum.hglC.name(), new EffectHglCHandler());
		handlerChain.put(EffectObjTypeEnum.sdwM.name(), new EffectSdwMHandler());
		handlerChain.put(EffectObjTypeEnum.sdwC.name(), new EffectSdwCHandler());
		handlerChain.put(EffectObjTypeEnum.sdwO.name(), new EffectSdwOHandler());
		handlerChain.put(EffectObjTypeEnum.bvlT.name(), new EffectBvlTHandler());
		handlerChain.put(EffectObjTypeEnum.bvlS.name(), new EffectBvlSHandler());
		handlerChain.put(EffectObjTypeEnum.uglg.name(), new EffectUglgHandler());
		handlerChain.put(EffectObjTypeEnum.lagl.name(), new EffectLfx2LaglHandler());
		handlerChain.put(EffectObjTypeEnum.Lald.name(), new EffectLaldHandler());
		handlerChain.put(EffectObjTypeEnum.srgR.name(), new EffectSrgRHandler());
		handlerChain.put(EffectObjTypeEnum.blur.name(), new EffectBlurHandler());
		handlerChain.put(EffectObjTypeEnum.bvlD.name(), new EffectBvlDHandler());
		handlerChain.put(EffectObjTypeEnum.TrnS.name(), new EffectTrnSObjcHandler());
		handlerChain.put(EffectObjTypeEnum.Sftn.name(), new EffectSftnHandler());
		handlerChain.put(EffectObjTypeEnum.MpgS.name(), new EffectMpgSHandler());
		handlerChain.put(EffectObjTypeEnum.AntA.name(), new EffectAntAHandler());
		handlerChain.put(EffectObjTypeEnum.Inpr.name(), new EffectInprHandler());
		handlerChain.put(EffectObjTypeEnum.InvT.name(), new EffectInvTHandler());
		handlerChain.put(EffectObjTypeEnum.Algn.name(), new EffectAlgnHandler());
		handlerChain.put(EffectObjTypeEnum.Scl.name(), new EffectSclHandler());
		handlerChain.put(EffectObjTypeEnum.Ptrn.name(), new EffectPtrnHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> data = new HashMap<String, Object>();
		
		String type = ByteUtil.readByteToStr(inputStream, 4);
		data.put("type", type);
		
		String name = ByteUtil.readByteToUnicodeStr(inputStream);
		data.put("name", name);
		
		String classId = ByteUtil.readByteToClassId(inputStream);
		data.put("classId", classId);
		
		int count = ByteUtil.readByteToInt(inputStream, 4); //这个应该是有多少个属性字段数量
		data.put("count", count);
		
		List<Map<String,Object>> properties = new ArrayList<>();
		for(int i = 0; i < count; i ++) {
			String id = ByteUtil.readByteToClassId(inputStream);
			IEffectObjHandler handler = handlerChain.get(id);
			if(handler != null) {
				Map<String, Object> info = handler.handle(inputStream);
				info.put("id", id);
				info.put("desc", EffectObjTypeEnum.bm(id));
				properties.add(info);
			}else {
				System.out.println("EffectLfx2EbblHandler【" + id + "】not found");
			}
		}
		return data;
	}
}
