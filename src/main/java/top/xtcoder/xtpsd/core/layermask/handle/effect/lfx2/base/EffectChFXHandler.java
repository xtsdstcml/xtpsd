package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 渐变色信息
 * @author xiangtao
 *
 */
public class EffectChFXHandler implements IEffectObjHandler{

	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Md.name(), new EffectMdHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.AntA.name(), new EffectAntAHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Invr.name(), new EffectInvrHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.lagl.name(), new EffectIaglHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Dstn.name(), new EffectDstnHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.blur.name(), new EffectBlurHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.MpgS.name(), new EffectMpgSHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", EffectObjTypeEnum.bm(objPropId));
				properties.add(propertie);
			}else {
				System.out.println("EffectChFXHandler【" + objPropId + "】not found");
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //'type'为类，'GlbC'为全局类
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
