package top.xtcoder.xtpsd.core.layermask.vo;

/**
 * 图层设置标记，每一位为一个标记，由右向左表示
 * @author xiangtao
 *
 */
public class LayerMaskParameterFlagInfo {
	
	private String flagStr;

	/**
	 * 透明度保护标记
	 */
	private int useMaskDensity;
	
	/**
	 * 显示图层标记
	 */
	private int userMaskFeather;
	
	/**
	 * 停用图层标记
	 */
	private int vectorMaskDensity;
	
	/**
	 * 适用于Photoshop 5.0及更高版本，告诉第4位是否有有用信息
	 */
	private int vectorMaskFeather;
	
	/**
	 * 与文档外观无关的像素数据
	 */
	private int documentPixData;
	
}
