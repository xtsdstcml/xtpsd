package top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto;

import top.xtcoder.xtpsd.exception.LayerEffectLfx2TypeException;

/**
 * 对象类型
 */
public enum EffectObjTypeEnum {
	//基础数据对象类型
	bool("bool", "布尔型对象"),
	_long("long", "整型对象"),
	doub("doub", "浮点型对象"),
	TEXT("TEXT", "Unicode文本型对象"),
	UntF("UntF", "单位值型对象"),
	_enum("enum", "枚举型对象"),
	alis("alis", "数据型"),
	type("type", "类和全局类对象"),
	Objc("Objc", "对象型对象"),
	VlLs("VlLs", "列表型对象"),
	obj("obj", "引用型对象"),
	prop("prop", "引用型对象 - Property对象"),
	Clss("Clss", "引用型对象 - 类"),
	Enmr("Enmr", "引用型对象 - 值来自枚举ID的枚举型属性对象"),
	rele("rele", "引用型对象 - 有值的普通属性对象"),
	Idnt("Idnt", "引用型对象 - 索引号属性对象"),
	indx("indx", "引用型对象 - 序号属性对象"),
	
	masterFXSwitch("masterFXSwitch", "显示效果图层"),		
	DrSh("DrSh", "投影效果图层"),
	IrSh("IrSh", "内阴影效果图层"),
	OrGl("OrGl", "外发光效果图层"),
	IrGl("IrGl", "内发光效果图层"),
	ebbl("ebbl", "斜面和浮雕效果图层"),
	ChFX("ChFX", "光泽效果图层"),
	SoFi("SoFi", "颜色叠加效果图层"),
	GrFl("GrFl", "渐变叠加效果图层"),
	patternFill("patternFill", "图案叠加效果图层"),
	FrFX("FrFX", "描边效果图层"),
	
	//颜色
	Rd("Rd", "红色"),
	Grn("Grn", "绿色"),
	Bl("Bl", "蓝色"),
	
	//等高线 类型
	Nm("Nm", "等高线名称"),
	Crv("Crv", "等高线曲线"),
	
	Hrzn("Hrzn", "输入(水平坐标)"),
	Vrtc("Vrtc", "输出(垂直坐标)"),
	Cnty("Cnty", "点角度标记"),
	
	//斜面和浮雕效果图层属性
	antialiasGloss("antialiasGloss", "阴影：光泽等高线消除锯齿标记"),
	useShape("useShape", "等高线启用标记"),
	useTexture("useTexture", "纹理启用标记"),
	textureDepth("textureDepth", "纹理：深度"),
	phase("phase", "纹理：方向"),
	enab("enab", "启用效果标记"),
	hglM("hglM", "阴影：高光模式"),
	hglC("hglC", "阴影：高光颜色"),
	hglO("hglO", "阴影：高光不透明度"),
	sdwM("sdwM", "阴影模式"),
	sdwC("sdwC", "阴影颜色"),
	sdwO("sdwO", "阴影不透明度"),
	bvlT("bvlT", "结构：斜面方法"),
	bvlS("bvlS", "结构：斜面和浮雕效果样式"),
	uglg("uglg", "阴影：使用全局光标记"),
	lagl("lagl", "阴影：光源角度"),
	Lald("Lald", "阴影：光源高度"),
	srgR("srgR", "结构：深度”"),
	bvlD("bvlD", "结构：方向"),
	Trns("Trns", "阴影：光泽等高线"),
	Sftn("Sftn", "结构：斜面柔和度（软化）"),
	MpgS("MpgS", "等高线"),
	InvT("InvT", "纹理：反相标记"),
	Ptrn("Ptrn", "纹理：图案信息"),
	
	GrdF("GrdF", "自定义渐变"),
	Intr("Intr", "平滑度"),
	Clrs("Clrs", "渐变色标信息"),
	
	Dstn("Dstn", "位移距离"),
	
	Clr("Clr", "颜色"),
	Lctn("Lctn", "颜色"),
	Mdpn("Mdpn", "中间点"),
	
	Md("Md", "混合模式类型"),
	//混合模式类型
	Nrml("Nrml", "正常"),
	Dslv("Dslv", "溶解"),
	Drkn("Drkn", "变暗"),
	Mltp("Mltp", "正片叠底"),
	CBrn("CBrn", "颜色加深"),
	Lghn("Lghn", "变亮"),
	Scrn("Scrn", "滤色"),
	CDdg("CDdg", "颜色减淡"),
	Ovrl("Ovrl", "叠加"),
	SftL("SftL", "柔光"),
	HrdL("HrdL", "强光"),
	Dfrn("Dfrn", "差值"),
	Xclu("Xclu", "排除"),
	H("H", "色相"),
	Strt("Strt", "饱和度"),
	Lmns("Lmns", "明度"),
	linearLight("linearLight", "线性光/线性明暗"),
	
	//投影效果图层
	layerConceals("layerConceals", "线性光/线性明暗"),
	
	//光泽效果图层
	Invr("Invr", "反相标记"),
	
	//描边效果图层
	Styl("Styl", "位置"),
	PntT("PntT", "颜色类型"),
	Sz("Sz", "描边宽度（大小）"),
	Lnkd("Lnkd", "颜色类型-渐变：方向"),
	
	Grad("Grad", "渐变色信息"),
	Type("Type", "渐变样式"),
	Rvrs("Rvrs", "反相标记"),
	Opct("Opct", "不透明度"),
	GlwT("GlwT", "柔化蒙版"),
	glwS("glwS", "光源类型标记"),
	Ckmt("Ckmt", "图层蒙版扩展"),
	blur("blur", "大小"),
	Nose("Nose", "杂色"),
	ShdN("ShdN", "抖动"),
	AntA("AntA", "消除锯齿标记"),
	Angl("Angl", "渐变角度"),
	Algn("Algn", "对齐标记"),
	Scl("Scl", "渐变大小（缩放）"),
	Ofst("Ofst", "渐变方向"),
	TrnS("TrnS", "等高线"),
	Inpr("Inpr", "范围"),
	Dthr("Dthr", "仿色标记"),
	;
	
	String value;
	String name;
	
	EffectObjTypeEnum(String value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public static EffectObjTypeEnum bm(String value) {
		EffectObjTypeEnum[] arr = EffectObjTypeEnum.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].value.equals(value)) {
				return arr[i];
			}
		}
		throw new LayerEffectLfx2TypeException("LayerEffectLfx2Type[" + value + " not found]");
	}
	
	@Override
	public String toString() {
		return value + ":" + name;
	}
}
