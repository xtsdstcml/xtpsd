package top.xtcoder.xtpsd.core.layermask.handle;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.ILayerEffectHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectClblHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectFxrpHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectIOpaHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectInfxHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectKnkoHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLclrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLfx2Handler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLnsrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLsctHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLspfHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLuniHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLyidHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectLyvrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectPhflHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectPlLdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectShmdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectSn2PHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectSoCoHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectTyShHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectVscgHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectVsmsHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectVstkHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.LayerEffectlrFXHandler;
import top.xtcoder.xtpsd.core.layermask.lang.EffectSet;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectHandle {
	private Log log = Log.getLog(LayerEffectHandle.class);
	
	private static Map<String, ILayerEffectHandler> leHandler = new HashMap<>();
	
	static {
		leHandler.put(EffectSet.luni.name(), new LayerEffectLuniHandler());
		leHandler.put(EffectSet.lyid.name(), new LayerEffectLyidHandler());
		leHandler.put(EffectSet.clbl.name(), new LayerEffectClblHandler());
		leHandler.put(EffectSet.infx.name(), new LayerEffectInfxHandler());
		leHandler.put(EffectSet.knko.name(), new LayerEffectKnkoHandler());
		leHandler.put(EffectSet.lspf.name(), new LayerEffectLspfHandler());
		leHandler.put(EffectSet.lclr.name(), new LayerEffectLclrHandler());
		leHandler.put(EffectSet.shmd.name(), new LayerEffectShmdHandler());
		leHandler.put(EffectSet.fxrp.name(), new LayerEffectFxrpHandler());
		leHandler.put(EffectSet.lnsr.name(), new LayerEffectLnsrHandler());
		leHandler.put(EffectSet.lrFX.name(), new LayerEffectlrFXHandler());
		leHandler.put(EffectSet.lfx2.name(), new LayerEffectLfx2Handler());
		leHandler.put(EffectSet.vscg.name(), new LayerEffectVscgHandler());
		leHandler.put(EffectSet.lyvr.name(), new LayerEffectLyvrHandler());
		leHandler.put(EffectSet.lsct.name(), new LayerEffectLsctHandler());
		leHandler.put(EffectSet.vsms.name(), new LayerEffectVsmsHandler());
		leHandler.put(EffectSet.vstk.name(), new LayerEffectVstkHandler());
		leHandler.put(EffectSet.PlLd.name(), new LayerEffectPlLdHandler());
		leHandler.put(EffectSet.SoLd.name(), new LayerEffectPlLdHandler());
		leHandler.put(EffectSet.sn2P.name(), new LayerEffectSn2PHandler());
		leHandler.put(EffectSet.SoCo.name(), new LayerEffectSoCoHandler());
		leHandler.put(EffectSet.iOpa.name(), new LayerEffectIOpaHandler());
		leHandler.put(EffectSet.phfl.name(), new LayerEffectPhflHandler());
		leHandler.put(EffectSet.TySh.name(), new LayerEffectTyShHandler());
	}
	
	public Map<String, Object> handle(EffectSet set, byte[] effectData) throws IOException {
		if(set == null) {
			return null;
		}
		try {
			ILayerEffectHandler h = leHandler.get(set.name());
			if(h != null) {
				Map<String, Object> data = h.handle(effectData);
				return data;
			}else {
				log.log("LayerEffectHandle【" + set + "】 not found");
				return null;
			}
		}catch(Exception e) {
			log.log(e, "LayerEffectHandle【" + set + "】 error :" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}