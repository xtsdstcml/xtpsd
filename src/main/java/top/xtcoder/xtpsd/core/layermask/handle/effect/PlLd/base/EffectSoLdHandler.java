package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectObjcHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectUntFHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectVILsHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lang.EffectObjHandleChain;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class EffectSoLdHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectSoLdHandler.class);
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		
		int version = ByteUtil.readByteToInt(inputStream, 4);
		map.put("version", version);
		int descriptorVersion = ByteUtil.readByteToInt(inputStream, 4);
		map.put("descriptorVersion", descriptorVersion);
		
//		ByteUtil.viewStreamByteInfo(log, "SoLD丢弃", "null", inputStream, 18);
		ByteUtil.readByteToStr(inputStream, 18);
		
		List<Map<String, Object>> properties = new ArrayList<>();
		while(inputStream.available() > 0) {
			String id = ByteUtil.readByteToClassId(inputStream);
			String type = ByteUtil.readByteToStr(inputStream, 4);
			IEffectObjHandler h = EffectObjHandleChain.get(type);
			if(h != null) {
				Map<String, Object> props = h.handle(inputStream);
				props.put("id", id);
				props.put("desc", LayerEffectPlLdType.bm(id));
				props.put("type", type);
				
				properties.add(props);
			}else {
				ByteUtil.viewStreamByteInfo(log, id, type, inputStream);
				//没有的话后面就没必要解析了，直接扔掉
				while(inputStream.read() != -1);
				break;
			}
		}
		map.put("properties", properties);
		return map;
	}

}
