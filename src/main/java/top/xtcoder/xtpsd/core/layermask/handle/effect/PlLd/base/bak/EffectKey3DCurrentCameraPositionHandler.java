package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeRawDataHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectKey3DCurrentCameraPositionHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectKey3DCurrentCameraPositionHandler.class);
	
	protected static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(LayerEffectPlLdType.key3DXPos.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DYPos.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DZPos.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DXAngle.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DYAngle.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DZAngle.name(), new EffectTypeDoubleHandler());
	}

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			IEffectObjHandler hc = handlerChain.get(objPropId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
				properties.add(propertie);
			}else {
				String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
				log.log("id[%s]还未解析，后面一段字节的信息=%s", objPropId, info);
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
