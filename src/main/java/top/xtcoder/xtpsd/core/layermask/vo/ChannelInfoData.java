package top.xtcoder.xtpsd.core.layermask.vo;

/**
 * Layer records的Channel数据
 * @author xiangtao
 *
 */
public class ChannelInfoData {
	/**
	 * 图层通道ID。
	 * 	在RGB图像中：0表示R通道，1表示G通道，2表示B通道；
	 * 	在CMYK图像中：0表示C通道，1表示M通道，2表示Y通道，3表示K通道，等等	；另外 -1表示透明蒙版，-2表示用户颜色通道。
	 */
	private int channelId;
	
	/**
	 * 图层通道数据长度
	 */
	private int channelDataLength;
	
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public int getChannelDataLength() {
		return channelDataLength;
	}
	public void setChannelDataLength(int channelDataLength) {
		this.channelDataLength = channelDataLength;
	}
}
