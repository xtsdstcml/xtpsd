package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LayerEffectLspfHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("lockTransparentImagePixelSign", effectData[0]);
		map.put("lockImagePixelSign", effectData[1]);
		map.put("lockPositionSign", effectData[2]);
		return map;
	}

}
