package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 描边效果图层为对象型
 */
public class EffectFrFXHandler  implements IEffectObjHandler{
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.phase.name(), new EffectLfx2PhaseHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Styl.name(), new EffectStylHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.PntT.name(), new EffectPntTHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Md.name(), new EffectMdHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Sz.name(), new EffectSzHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Grad.name(), new EffectGradHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Angl.name(), new EffectAnglHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Type.name(), new EffectTypeHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Rvrs.name(), new EffectRvrsHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Scl.name(), new EffectSclHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Algn.name(), new EffectAlgnHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Ofst.name(), new EffectLfx2OfstHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Ptrn.name(), new EffectPtrnHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Lnkd.name(), new EffectLnkdHandler());
		
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", EffectObjTypeEnum.bm(objPropId));
				properties.add(propertie);
			}else {
				System.out.println("EffectFrFXHandler【" + objPropId + "】 not found");
			}
		}
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //'type'为类，'GlbC'为全局类
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
