package top.xtcoder.xtpsd.core.layermask.handle.effect.lang;

/**
 * https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/#50577411_21585
 */
public enum BasePropType {
	TEXT("TEXT", "Unicode文本型"),
	_long("long", "整型"),
	doub("doub", "浮点型"),
	_enum("enum", "枚举型"),
	bool("bool", "布尔型"),
	UntF("UntF", "单位值型"),
	comp("comp", "长整形"),
	obj("obj", "引用型对象"),
	VlLs("VlLs", "列表型对象"),
	Objc("Objc", "对象型对象 - 类"),
	GlbO("GlbO", "对象型对象 - 全局类"),
	type("type", "引用型对象 - 类"),
	GlbC("GlbC", "引用型对象 - 全局类"),
	alis("alis", "数据型"),
	tdta("tdta", "Raw Data"),
	;
	
	String key;
	String label;
	
	BasePropType(String key, String label) {
		this.key = key;
		this.label = label;
	}
	
	public static BasePropType bm(String key) {
		BasePropType[] arr = BasePropType.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].key.equals(key)) {
				return arr[i];
			}
		}
		throw new ObjPropTypeException("ObjPropType[" + key + " not found]");
	}
	
	public String key() {
		return key;
	}
	
	public String label() {
		return label;
	}
	
	@Override
	public String toString() {
		return key + ":" + label;
	}
}
