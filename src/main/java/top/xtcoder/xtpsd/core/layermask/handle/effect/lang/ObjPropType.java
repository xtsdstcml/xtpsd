package top.xtcoder.xtpsd.core.layermask.handle.effect.lang;

/**
 * 引用型对象的属性类型
 */
public enum ObjPropType {
	prop("prop", "Property对象"),
	Clss("Clss", "类"),
	Enmr("Enmr", "枚举"),
	rele("rele", "值"),
	Idnt("Idnt", "索引号"),
	indx("indx", "序号"),
	name("name", "Unicode名称"),
	;
	
	String key;
	String label;
	
	ObjPropType(String key, String label) {
		this.key = key;
		this.label = label;
	}
	
	public static ObjPropType bm(String key) {
		ObjPropType[] arr = ObjPropType.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].key.equals(key)) {
				return arr[i];
			}
		}
		throw new ObjPropTypeException("ObjPropType[" + key + " not found]");
	}
	
	public String key() {
		return key;
	}
	
	public String label() {
		return label;
	}
	
	@Override
	public String toString() {
		return key + ":" + label;
	}
}
