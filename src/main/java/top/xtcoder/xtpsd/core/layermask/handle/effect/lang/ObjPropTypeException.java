package top.xtcoder.xtpsd.core.layermask.handle.effect.lang;

public class ObjPropTypeException extends RuntimeException{
	public ObjPropTypeException(String msg) {
		super(msg);
	}
}