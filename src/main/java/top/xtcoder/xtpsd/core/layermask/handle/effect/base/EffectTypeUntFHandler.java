package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 单位值型对象:
 * @author xiangtao
 *
 */
public class EffectTypeUntFHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		String unit = ByteUtil.readByteToStr(inputStream, 4);
		double value = ByteUtil.readByteToDouble8(inputStream);
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("unit", unit);
		if("#Ang".equals(unit)) {
			map.put("unitDesc", "角度：基本度数");
		}else if("#Rsl".equals(unit)) {
			map.put("unitDesc", "密度：每英寸为基本单位");
		}else if("#Rlt".equals(unit)) {
			map.put("unitDesc", "距离：基于72ppi");
		}else if("#Nne".equals(unit)) {
			map.put("unitDesc", "无");
		}else if("#Prc".equals(unit)) {
			map.put("unitDesc", "百分比：单位值");
		}else if("#Pxl".equals(unit)) {
			map.put("unitDesc", "像素：标记的单位值");
		}
		map.put("value", value);
		return map;
	}

}
