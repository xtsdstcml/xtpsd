package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;

/**
 * “结构：颜色类型”为枚举型
 * @author xiangtao
 *
 */
public class EffectPntTHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		Map<String, Object> map = super.handle(inputStream);
		String name = (String) map.get("enumName");
		if("SClr".equals(name)) {
			map.put("label", "颜色");
		}else if("GrFl".equals(name)) {
			map.put("label", "渐变");
		}else if("Ptrn".equals(name)) {
			map.put("label", "图案");
		}
		return map;
	}
}
