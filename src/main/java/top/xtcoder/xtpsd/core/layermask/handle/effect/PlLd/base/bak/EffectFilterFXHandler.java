package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectFilterFXHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectFilterFXHandler.class);
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(LayerEffectPlLdType.enab.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.validAtPosition.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterMaskEnable.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterMaskLinked.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterMaskExtendWithWhite.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterMaskExtendWithWhite.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterFXList.name(), new EffectFilterFXListHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
			try {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
				properties.add(propertie);
			}catch(Exception e) {
				String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
				log.log(e, "id[%s]还未解析，后面一段字节的信息=%s", objPropId, info);
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
