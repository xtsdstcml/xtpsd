package top.xtcoder.xtpsd.core.layermask.lang;

import top.xtcoder.xtpsd.exception.EffectSetNotFoundException;

public enum EffectSet {
	levl("levl", "色阶"),		
	curv("curv", "曲线"),	
	luni("luni", "图层Unicode名字"),
	cust("cust", "cust"),
	shmd("shmd", "shmd"),
	brit("brit", "亮度/对比度"),		
	blnc("blnc", "色彩平衡"),
	hue2("hue2", "色相/饱和度"),	
	selc("selc", "可选颜色"),	
	thrs("thrs", "阈值调整"),		
	nvrt("nvrt", "反相调整"),		
	post("post", "色调分离"),
	mixr("mixr", "通道混合器"),
	grdm("grdm", "渐变映射"),		
	phfl("phfl", "照片滤镜"),	
	lrFX("lrFX", "效果图层1"),	
	lfx2("lfx2", "效果图层2"),
	tySh("tySh", "文本图层"),
	TySh("TySh", "文本图层 - 对象"),
	SoCo("SoCo", "纯色填充"),
	GdFl("GdFl", "渐变填充"),
	PtFl("PtFl", "图案填充"),	
	lnsr("lnsr", "图层名称"),
	lyid("lyid", "图层ID"),
	clbl("clbl", "图层剪贴混合标记"),		
	infx("infx", "图层内部混合标记"),
	knko("knko", "锁定"),
	lspf("lspf", "保护设置"),
	lclr("lclr", "图层注释颜色"),
	fxrp("fxrp", "图层参考点"),
	lyvr("lyvr", "图层版本号"),
	tsly("tsly", "透明形状图层标记"),
	lmgm("lmgm", "图层蒙版隐藏效果标记"),
	vmgm("vmgm", "矢量蒙版隐藏效果标记"),
	iOpa("iOpa", "图层填充不透明度"),
	lsct("lsct", "图层类型"),
	brst("brst", "禁用图层通道设置"),
	vscg("vscg", "矢量笔划内容数据"),
	vmsk("vmsk", "矢量图层蒙版"),
	vsms("vsms", "矢量遮罩设置"),
	vstk("vstk", "矢量笔画数据"),
	PlLd("PlLd", "放置图层"),
	SoLd("SoLd", "放置图层"),
	sn2P("sn2P", "使用对齐渲染"),
	
	;
	
	String value;
	String name;
	
	EffectSet(String value, String name) {
		this.value = value;
		this.name = name;
	}
	
	public static EffectSet bm(String value) {
		EffectSet[] arr = EffectSet.values();
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].value.equals(value)) {
				return arr[i];
			}
		}
		throw new EffectSetNotFoundException("EffectSet["+value+" not found]");
	}
	
	@Override
	public String toString() {
		return value + ":" + name;
	}
}
