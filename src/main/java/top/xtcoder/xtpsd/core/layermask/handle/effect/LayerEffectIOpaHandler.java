package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 *  图层填充不透明度
 * @author xiangtao
 *
 */
public class LayerEffectIOpaHandler implements ILayerEffectHandler{
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			int opacity = ByteUtil.readByteToInt(inputStream, 1);
			map.put("opacity", opacity);
			return map;
		}
	}
}
