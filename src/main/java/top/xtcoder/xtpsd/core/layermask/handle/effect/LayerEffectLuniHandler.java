package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

public class LayerEffectLuniHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			int unicodeLength = ByteUtil.readByteToInt(inputStream, 4);
			String name = ByteUtil.readByteToUnicodeStr(inputStream, unicodeLength * 2);
			Map<String, Object> map = new HashMap<>();
			map.put("length", unicodeLength);
			map.put("name", name);
			return map;
		}
		
	}

}
