package top.xtcoder.xtpsd.core.layermask.vo;

public class PsdLayerAndMaskInfomation {
	/**
	 * 数据长度
	 */
	private int length;
	
	/**
	 * 图层信息
	 */
	private LayerInfo layerInfo;
	
	/**
	 * 蒙版信息
	 */
	private LayerMaskInfo maskInfo;
	
	/**
	 * (Photoshop 4.0 或者最高) 包含各种类型数据的一系列标记块
	 */
	private TaggedBlock taggedBlocks;

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public LayerInfo getLayerInfo() {
		return layerInfo;
	}

	public void setLayerInfo(LayerInfo layerInfo) {
		this.layerInfo = layerInfo;
	}

	public LayerMaskInfo getMaskInfo() {
		return maskInfo;
	}

	public void setMaskInfo(LayerMaskInfo maskInfo) {
		this.maskInfo = maskInfo;
	}

	public TaggedBlock getTaggedBlocks() {
		return taggedBlocks;
	}

	public void setTaggedBlocks(TaggedBlock taggedBlocks) {
		this.taggedBlocks = taggedBlocks;
	}
	
	
}
