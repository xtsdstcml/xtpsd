package top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface IEffectObjHandler {
	Map<String, Object> handle(InputStream inputStream) throws IOException ;
}
