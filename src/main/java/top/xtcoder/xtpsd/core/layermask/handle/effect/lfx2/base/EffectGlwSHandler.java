package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 图素：光源类型标记
 * @author xiangtao
 *
 */
public class EffectGlwSHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		
		Map<String, Object> map = super.handle(inputStream);
		String name = (String) map.get("enumName");
		if("SrcC".equals(name)) {
			map.put("label", "从中心发光照亮");
		}else if("SrcE".equals(name)) {
			map.put("label", "从边缘发光照亮");
		}else {
			map.put("label", "未知");
		}
		return map;
	}
}
