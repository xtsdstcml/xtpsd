package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 浮点型对象
 * @author xiangtao
 *
 */
public class EffectTypeDoubleHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		double value = ByteUtil.readByteToDouble8(inputStream);
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("value", value);
		return map;
	}

}
