package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectAntAHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBlurHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectCkmtHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectClrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectEnabHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectGlwTHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectGradHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectInprHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectNoseHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectOpctHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectShdNHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectTrnSObjcHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class EffectLfx2OrGlHandler implements IEffectObjHandler{
	
	private static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Md.name(), new EffectMdHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Grad.name(), new EffectGradHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.GlwT.name(), new EffectGlwTHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Ckmt.name(), new EffectCkmtHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.blur.name(), new EffectBlurHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Nose.name(), new EffectNoseHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.ShdN.name(), new EffectShdNHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.AntA.name(), new EffectAntAHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.TrnS.name(), new EffectTrnSObjcHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Inpr.name(), new EffectInprHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int propIdLength = ByteUtil.readByteToInt(inputStream, 4);
			propIdLength = propIdLength < 4 ? 4 : propIdLength;
			String propId = ByteUtil.readByteToStr(inputStream, propIdLength);
			IEffectObjHandler hc = lfx2HandlerChain.get(propId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", propId);
				propertie.put("desc", EffectObjTypeEnum.bm(propId));
				properties.add(propertie);
			}else {
				System.out.println("EffectLfx2OrGlHandler." + i + "【" + propId + "】 not found");
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("nameLength", nameLength);
		map.put("classId", classId);
		map.put("name", name);
		map.put("properties", properties);
		return map;
	}

}
