package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 *  “渐变色标信息”为列表型对象
 * @author xiangtao
 *
 */
public class EffectClrsHandler implements IEffectObjHandler{

	protected static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		handlerChain.put(EffectObjTypeEnum.Type.name(), new EffectTypeHandler());
		handlerChain.put(EffectObjTypeEnum.Lctn.name(), new EffectLctnHandler());
		handlerChain.put(EffectObjTypeEnum.Mdpn.name(), new EffectMdpnHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> objects = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			Map<String, Object> object = new HashMap<String, Object>();
			String objType = ByteUtil.readByteToStr(inputStream, 4);
			String objName = ByteUtil.readByteToUnicodeStr(inputStream);
			String objClassid = ByteUtil.readByteToClassId(inputStream);
			int objPropCount = ByteUtil.readByteToInt(inputStream, 4);
			
			object.put("type", objType);
			object.put("name", objName);
			object.put("classId", objClassid);
			object.put("count", objPropCount);
			for(int propIndex = 0; propIndex < objPropCount; propIndex ++) {
				int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
				objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
				String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
				
				Map<String, Object> props = handlerChain.get(objPropId).handle(inputStream);
				props.put("id", objPropId);
				props.put("desc", EffectObjTypeEnum.bm(objPropId));
				object.put("properties", props);
			}
			objects.add(object);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("count", count);
		map.put("objects", objects);
		return map;
	}
	
}
