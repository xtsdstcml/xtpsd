package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 内发光效果，效果签名为'iglw'
 * @author xiangtao
 *
 */
public class EffectIglwHandler implements IEffectIrFXHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int dataLength = ByteUtil.readByteToInt(inputStream, 4);
		int version = ByteUtil.readByteToInt(inputStream, 4);
		
		//发光大小
		int lightSize = ByteUtil.readByteToInt(inputStream, 4);
		
		//图层蒙版扩展（0-100）
		int layerMaskExtension = ByteUtil.readByteToInt(inputStream, 4);
		
		//发光颜色
		int lightColorType = ByteUtil.readByteToInt(inputStream, 2);
		int lightColor1 = ByteUtil.readByteToInt(inputStream, 2);
		int lightColor2 = ByteUtil.readByteToInt(inputStream, 2);
		int lightColor3 = ByteUtil.readByteToInt(inputStream, 2);
		int lightColor4 = ByteUtil.readByteToInt(inputStream, 2);
		
		String sign = ByteUtil.readByteToStr(inputStream, 4);
		String blendType = ByteUtil.readByteToStr(inputStream, 4);
		int enable = ByteUtil.readByteToInt(inputStream, 1);
		int opacity = ByteUtil.readByteToInt(inputStream, 1);
		//光源类型标记，0为从边缘发光照亮，1为从中心发光照亮
		int lightOrginSign = ByteUtil.readByteToInt(inputStream, 1);
		
		//天然颜色-颜色空间类型
		int naturalColorType = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor1 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor2 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor3 = ByteUtil.readByteToInt(inputStream, 2);
		int naturalColor4 = ByteUtil.readByteToInt(inputStream, 2);
		
		Map<String, Object> map = new HashMap<>();
		map.put("dataLength", dataLength);
		map.put("version", version);
		map.put("lightSize", lightSize);
		map.put("layerMaskExtension", layerMaskExtension);
		
		map.put("lightColorType", lightColorType);
		map.put("lightColor1", lightColor1);
		map.put("lightColor2", lightColor2);
		map.put("lightColor3", lightColor3);
		map.put("lightColor4", lightColor4);
		
		map.put("sign", sign);
		map.put("blendType", blendType);
		map.put("enable", enable);
		map.put("opacity", opacity);
		map.put("lightOrginSign", lightOrginSign);
		
		map.put("naturalColorType", naturalColorType);
		map.put("naturalColor1", naturalColor1);
		map.put("naturalColor2", naturalColor2);
		map.put("naturalColor3", naturalColor3);
		map.put("naturalColor4", naturalColor4);
		return map;
	}
}
