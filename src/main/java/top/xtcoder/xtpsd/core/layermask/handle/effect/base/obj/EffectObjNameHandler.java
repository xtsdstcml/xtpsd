package top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 引用型对象 - Unicode名称对象
 * @author xiangtao
 * 
 */
public class EffectObjNameHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int length = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, length * 2);
		
		Map<String, Object> map = new HashMap<>();
		map.put("name", name);
		return map;
	}
}