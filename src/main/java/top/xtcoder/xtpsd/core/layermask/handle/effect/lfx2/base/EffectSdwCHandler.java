package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * 阴影：阴影颜色
 * @author xiangtao
 *
 */
public class EffectSdwCHandler extends EffectRgbcHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		return map;
	}
}
