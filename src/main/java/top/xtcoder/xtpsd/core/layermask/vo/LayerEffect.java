package top.xtcoder.xtpsd.core.layermask.vo;

import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.lang.EffectSet;

public class LayerEffect {
	/**
	 * 图层附加效果层标记，8BIM' 或者 '8B64'
	 */
	private String signature;
	
	/**
	 * 图层附加效果层标签
	 */
	private String label;
	
	/**
	 * 图层附加效果层数据长度
	 */
	private int length;
	
	
	/**
	 * 图层附加效果层设置信息
	 */
	private EffectSet effectSet;
	
	/**
	 * 效果信息
	 */
	private Map<String, Object> effectInfo;
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public EffectSet getEffectSet() {
		return effectSet;
	}

	public void setEffectSet(EffectSet effectSet) {
		this.effectSet = effectSet;
	}

	public Map<String, Object> getEffectInfo() {
		return effectInfo;
	}

	public void setEffectInfo(Map<String, Object> effectInfo) {
		this.effectInfo = effectInfo;
	}
}
