package top.xtcoder.xtpsd.core.layermask.vo;

import top.xtcoder.xtpsd.core.layermask.lang.BlendMode;

public class BlendModeInfo {
	/**
	 * 混合模式签名，总是为'8BIM'
	 */
	private String signature;
	
	/**
	 * 混合模式类型
	 */
	private String key;
	
	/**
	 * 混合模式类型
	 */
	private BlendMode mode;
	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public BlendMode getMode() {
		return mode;
	}
	public void setMode(BlendMode mode) {
		this.mode = mode;
	}
	
}
