package top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.IrFX.IEffectIrFXHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 斜面和浮雕效果，效果签名为'bevl'
 * @author xiangtao
 * 
 */
public class EffectBevlHandler implements IEffectIrFXHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = new HashMap<>();
		
		int dataLength = ByteUtil.readByteToInt(inputStream, 4);
		map.put("dataLength", dataLength);
		
		int version = ByteUtil.readByteToInt(inputStream, 4);
		map.put("version", version);
		
		//光源角度
		int lightAngle = ByteUtil.readByteToInt(inputStream, 4);
		map.put("lightAngle", lightAngle);
		
		//斜面大小
		int bevelSize = ByteUtil.readByteToInt(inputStream, 4);
		map.put("bevelSize", bevelSize);
		
		//斜面柔和度（软化）
		int bevelSoftness = ByteUtil.readByteToInt(inputStream, 4);
		map.put("bevelSoftness", bevelSoftness);
		
		//高光模式签名
		String highLightSign = ByteUtil.readByteToStr(inputStream, 4);
		map.put("highLightSign", highLightSign);
		
		//高光模式类型（见混合模式类型）
		String hightLightType = ByteUtil.readByteToStr(inputStream, 4);
		map.put("hightLightType", hightLightType);
		
		//阴影模式签名
		String shadowSign = ByteUtil.readByteToStr(inputStream, 4);
		map.put("shadowSign", shadowSign);
		
		//阴影模式类型（见混合模式类型）
		String shadowType = ByteUtil.readByteToStr(inputStream, 4);
		map.put("shadowType", shadowType);
		
		//高光颜色
		int lightColorType = ByteUtil.readByteToInt(inputStream, 2);
		map.put("lightColorType", lightColorType);
		int lightColor1 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("lightColor1", lightColor1);
		int lightColor2 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("lightColor2", lightColor2);
		int lightColor3 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("lightColor3", lightColor3);
		int lightColor4 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("lightColor4", lightColor4);
		
		//阴影颜色
		int shadowColorType = ByteUtil.readByteToInt(inputStream, 2);
		map.put("shadowColorType", shadowColorType);
		int shadowColor1 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("shadowColor1", shadowColor1);
		int shadowColor2 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("shadowColor2", shadowColor2);
		int shadowColor3 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("shadowColor3", shadowColor3);
		int shadowColor4 = ByteUtil.readByteToInt(inputStream, 2);
		map.put("shadowColor4", shadowColor4);
		
		//斜面和浮雕效果样式，0为外斜面、1为内斜面、2为浮雕效果、3为枕状浮雕、4为描边浮雕
		int bevelType = ByteUtil.readByteToInt(inputStream, 1);
		map.put("bevelType", bevelType);
		
		//阴影：高光不透明度
		int hightOpacity = ByteUtil.readByteToInt(inputStream, 1);
		map.put("hightOpacity", hightOpacity);
		//阴影：阴影不透明度
		int shadowOpacity = ByteUtil.readByteToInt(inputStream, 1);
		map.put("shadowOpacity", shadowOpacity);
		//启用效果标记
		int enable = ByteUtil.readByteToInt(inputStream, 1);
		map.put("enable", enable);
		//阴影：使用全局光标记
		int shadowUseGloablLightSign = ByteUtil.readByteToInt(inputStream, 1);
		map.put("shadowUseGloablLightSign", shadowUseGloablLightSign);
		//结构：方向，0为上，1为下
		int direction = ByteUtil.readByteToInt(inputStream, 1);
		map.put("direction", direction);
		
		if(version == 2) {
			//阴影：真实高光颜色（2字节颜色空间类型 + 4*2字节颜色值）
			int naturalLightColorType = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalLightColorType", naturalLightColorType);
			int naturalLightColor1 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalLightColor1", naturalLightColor1);
			int naturalLightColor2 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalLightColor2", naturalLightColor2);
			int naturalLightColor3 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalLightColor3", naturalLightColor3);
			int naturalLightColor4 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalLightColor4", naturalLightColor4);
			
			//阴影：真实阴影颜色（2字节颜色空间类型 + 4*2字节颜色值）
			int naturalShadowColorType = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalShadowColorType", naturalShadowColorType);
			int naturalShadowColor1 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalShadowColor1", naturalShadowColor1);
			int naturalShadowColor2 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalShadowColor2", naturalShadowColor2);
			int naturalShadowColor3 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalShadowColor3", naturalShadowColor3);
			int naturalShadowColor4 = ByteUtil.readByteToInt(inputStream, 2);
			map.put("naturalShadowColor4", naturalShadowColor4);
		}
		return map;
	}
}
