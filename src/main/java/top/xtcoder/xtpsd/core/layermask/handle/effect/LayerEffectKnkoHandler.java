package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LayerEffectKnkoHandler implements ILayerEffectHandler{

	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("sign", effectData[0]);
		return map;
	}

}
