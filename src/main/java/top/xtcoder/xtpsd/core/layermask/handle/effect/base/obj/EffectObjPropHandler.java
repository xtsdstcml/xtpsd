package top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 引用型对象 - Unicode名称对象
 * @author xiangtao
 * 
 */
public class EffectObjPropHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String name = ByteUtil.readByteToUnicodeStr(inputStream);
		String classId = ByteUtil.readByteToClassId(inputStream);
		String keyId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> map = new HashMap<>();
		map.put("name", name);
		map.put("keyId", keyId);
		return map;
	}

}
