package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectK3DLightsHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectK3DLightsHandler.class);
	
	protected static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(LayerEffectPlLdType.Nm.name(), new EffectTypeTextHandler());
		handlerChain.put(LayerEffectPlLdType.Rd.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.Grn.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.Bl.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.hots.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.FlOf.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.atta.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.attb.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.attc.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.orad.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.irad.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.mult.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.ssml.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.afpw.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.shdw.name(), new EffectTypeLongHandler());
		handlerChain.put(LayerEffectPlLdType.attn.name(), new EffectTypeBoolHandler());
		handlerChain.put(LayerEffectPlLdType.afon.name(), new EffectTypeBoolHandler());
		handlerChain.put(LayerEffectPlLdType.ison.name(), new EffectTypeBoolHandler());
		handlerChain.put(LayerEffectPlLdType.attt.name(), new EffectTypeLongHandler());
		handlerChain.put(LayerEffectPlLdType.Type.name(), new EffectTypeLongHandler());
		handlerChain.put(LayerEffectPlLdType.key3DMatrix.name(), new EffectKey3DMatrixHandler());
		handlerChain.put(LayerEffectPlLdType.X.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.Y.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.Z.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.tarx.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.tary.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.tarz.name(), new EffectTypeDoubleHandler());
		handlerChain.put(LayerEffectPlLdType.key3DPosition.name(), new EffectKey3DPositionHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			Map<String, Object> item = new HashMap<>();
			
			String objtype = ByteUtil.readByteToStr(inputStream, 4);
			item.put("type", objtype);
			
			String objName = ByteUtil.readByteToUnicodeStr(inputStream);
			item.put("name", objName);
			
			String objClassId = ByteUtil.readByteToClassId(inputStream);
			item.put("classId", objClassId);
			
			int propCount = ByteUtil.readByteToInt(inputStream, 4);
			item.put("count", propCount);
			
			List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
			for(int j = 0; j < propCount; j ++) {
				int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
				objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
				String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
				IEffectObjHandler hc = handlerChain.get(objPropId);
				if(hc != null) {
					Map<String, Object> propertie = hc.handle(inputStream);
					propertie.put("id", objPropId);
					propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
					properties.add(propertie);
				}else {
					String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
					log.log("id[%s]还未解析，后面一段字节的信息=%s", objPropId, info);
				}
			}
			item.put("properties", properties);
			list.add(item);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //'type'为'VlLs'为列表型对象
		map.put("count", count);
		map.put("list", list);
		return map;
	}
}
