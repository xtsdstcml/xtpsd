package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;

/**
 * 颜色类型-图案：与图层链接标记”为布尔型
 * @author xiangtao
 *
 */
public class EffectLnkdHandler extends EffectTypeBoolHandler{

}
