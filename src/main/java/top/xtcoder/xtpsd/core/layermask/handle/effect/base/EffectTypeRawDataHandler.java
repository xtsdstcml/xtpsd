package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 原始数据 - 待解析
 * @author xiangtao
 *
 */
public class EffectTypeRawDataHandler implements IEffectObjHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int length = ByteUtil.readByteToInt(inputStream, 1);
		String sss = ByteUtil.readByteToStr(inputStream, length);
//		double value = ByteUtil.readByteToDouble8(inputStream);
		System.out.println("rawData=" + length);
		System.out.println("sss=" + sss);
		Map<String, Object> map = new HashMap<>();
//		map.put("type", type);
//		map.put("value", value);
		return map;
	}

}
