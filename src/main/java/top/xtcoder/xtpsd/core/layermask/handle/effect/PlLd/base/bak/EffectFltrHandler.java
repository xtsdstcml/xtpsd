package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeUnicodeHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectDstnHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMdHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectFltrHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectFltrHandler.class);
	
	
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(LayerEffectPlLdType.FlRs.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.k3DLights.name(), new EffectK3DLightsHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.key3DCurrentCameraPosition.name(), new EffectKey3DCurrentCameraPositionHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Glos.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Mtrl.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Exps.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.AmbB.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.AmbC.name(), new EffectAmbCHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.BmpA.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.BmpC.name(), new EffectBmpCHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType._name.n(), new EffectTypeUnicodeHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Wdth.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Hght.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterID.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Angl.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Dstn.name(), new EffectDstnHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.WndM.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Drct.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.GEfk.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.FbrL.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Brgh.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Cntr.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.FlRs.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.IntE.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.IntC.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Lvl.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Edg.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Amnt.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Md.name(), new EffectMdHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.TlNm.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.TlOf.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.FlCl.name(), new EffectTypeEnumHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.ExtS.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.ExtD.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.ExtF.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.ExtM.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.ExtR.name(), new EffectTypeEnumHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
//			System.out.println("objPropIdLength." + i + "=" + objPropIdLength);
//			System.out.println("objPropId." + i + "=" + objPropId);
			IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
				properties.add(propertie);
			}else {
				String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
				log.log("id[%s]还未解析，后面一段字节的信息=%s", objPropId, info);
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type); //
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
