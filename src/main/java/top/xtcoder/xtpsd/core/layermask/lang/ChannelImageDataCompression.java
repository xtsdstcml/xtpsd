package top.xtcoder.xtpsd.core.layermask.lang;

public enum ChannelImageDataCompression {
	RawData(0),
	RLE(1),
	ZIPWithoutPrediction(2),
	ZIPWithPrediction(3);
	
	int value;
	ChannelImageDataCompression(int v) {
		value = v;
	}
	
	public int value() {
		return value;
	}
}
