package top.xtcoder.xtpsd.core.layermask.vo;

import java.util.List;

public class ChannelInformation {
	/**
	 * 通道数
	 */
	private int channelNumber;
	
	/**
	 * 通道数据
	 */
	private List<ChannelInfoData> channelDatas;
	
	public int getChannelNumber() {
		return channelNumber;
	}
	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}
	public List<ChannelInfoData> getChannelDatas() {
		return channelDatas;
	}
	public void setChannelDatas(List<ChannelInfoData> channelDatas) {
		this.channelDatas = channelDatas;
	}
}
