package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 矢量遮罩设置
 * @author xiangtao
 *
 */
public class LayerEffectVsmsHandler implements ILayerEffectHandler{
	
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			int version = ByteUtil.readByteToInt(inputStream, 4);
			int flag = ByteUtil.readByteToInt(inputStream, 4);
			//解析矢量遮罩设置
			System.err.println("LayerEffectVsmsHandler[矢量遮罩设置] 还未解析");
			return map;
		}
	}
}
