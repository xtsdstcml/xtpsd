package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectAntAHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectBlurHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectCkmtHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectClrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectEnabHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectGlwSHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectGlwTHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectGradHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectInprHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectMdHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectNoseHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectOpctHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectShdNHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base.EffectTrnSObjcHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 内发光效果图层
 * @author xiangtao
 *
 */
public class EffectLfx2IrGlHandler implements IEffectObjHandler {
	private static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(EffectObjTypeEnum.enab.name(), new EffectEnabHandler());
		handlerChain.put(EffectObjTypeEnum.Md.name(), new EffectMdHandler());
		handlerChain.put(EffectObjTypeEnum.Clr.name(), new EffectClrHandler());
		handlerChain.put(EffectObjTypeEnum.Grad.name(), new EffectGradHandler());
		handlerChain.put(EffectObjTypeEnum.Opct.name(), new EffectOpctHandler());
		handlerChain.put(EffectObjTypeEnum.GlwT.name(), new EffectGlwTHandler());
		handlerChain.put(EffectObjTypeEnum.Ckmt.name(), new EffectCkmtHandler());
		handlerChain.put(EffectObjTypeEnum.blur.name(), new EffectBlurHandler());
		handlerChain.put(EffectObjTypeEnum.ShdN.name(), new EffectShdNHandler());
		handlerChain.put(EffectObjTypeEnum.Nose.name(), new EffectNoseHandler());
		handlerChain.put(EffectObjTypeEnum.AntA.name(), new EffectAntAHandler());
		handlerChain.put(EffectObjTypeEnum.glwS.name(), new EffectGlwSHandler());
		handlerChain.put(EffectObjTypeEnum.TrnS.name(), new EffectTrnSObjcHandler());
		handlerChain.put(EffectObjTypeEnum.Inpr.name(), new EffectInprHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> data = new HashMap<String, Object>();
		
		String type = ByteUtil.readByteToStr(inputStream, 4);
		data.put("type", type);
		
		String name = ByteUtil.readByteToUnicodeStr(inputStream);
		data.put("name", name);
		
		String classId = ByteUtil.readByteToClassId(inputStream);
		data.put("classId", classId);
		
		int count = ByteUtil.readByteToInt(inputStream, 4); //这个应该是有多少个属性字段数量
		data.put("count", count);
		
		String enabId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> enabInfo = handlerChain.get(enabId).handle(inputStream);
		enabInfo.put("id", enabId);
		enabInfo.put("desc", EffectObjTypeEnum.bm(enabId));
		data.put("enabInfo", enabInfo);
		
		String mdId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> mdInfo = handlerChain.get(mdId).handle(inputStream);
		mdInfo.put("id", mdId);
		mdInfo.put("desc", EffectObjTypeEnum.bm(mdId));
		data.put("colorOverlayStyle", mdInfo);
		
		String clrId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> clrInfo = handlerChain.get(clrId).handle(inputStream);
		clrInfo.put("id", clrId);
		clrInfo.put("desc", EffectObjTypeEnum.bm(clrId));
		data.put("clrColor", clrInfo);
		
		String gradId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> gradInfo = handlerChain.get(gradId).handle(inputStream);
		gradInfo.put("id", gradId);
		gradInfo.put("desc", EffectObjTypeEnum.bm(gradId));
		data.put("gradColor", gradInfo);
		
		String opctId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> opctInfo = handlerChain.get(opctId).handle(inputStream);
		opctInfo.put("id", opctId);
		opctInfo.put("desc", EffectObjTypeEnum.bm(opctId));
		data.put("opacity", opctInfo);
		
		String glwtId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> glwtInfo = handlerChain.get(glwtId).handle(inputStream);
		glwtInfo.put("id", glwtId);
		glwtInfo.put("desc", EffectObjTypeEnum.bm(glwtId));
		data.put("glwtInfo", glwtInfo);
		
		String ckmtId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> ckmtInfo = handlerChain.get(ckmtId).handle(inputStream);
		ckmtInfo.put("id", ckmtId);
		ckmtInfo.put("desc", EffectObjTypeEnum.bm(ckmtId));
		data.put("ckmtInfo", ckmtInfo);
		
		String blurId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> blurInfo = handlerChain.get(blurId).handle(inputStream);
		blurInfo.put("id", blurId);
		blurInfo.put("desc", EffectObjTypeEnum.bm(blurId));
		data.put("blurInfo", blurInfo);
		
		String shdnId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> shdnInfo = handlerChain.get(shdnId).handle(inputStream);
		shdnInfo.put("id", shdnId);
		shdnInfo.put("desc", EffectObjTypeEnum.bm(shdnId));
		data.put("shdnInfo", shdnInfo);
		
		String antaId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> antaInfo = handlerChain.get(antaId).handle(inputStream);
		antaInfo.put("id", antaId);
		antaInfo.put("desc", EffectObjTypeEnum.bm(antaId));
		data.put("antaInfo", antaInfo);
		
		String glwsId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> glwsInfo = handlerChain.get(glwsId).handle(inputStream);
		glwsInfo.put("id", glwsId);
		glwsInfo.put("desc", EffectObjTypeEnum.bm(glwsId));
		data.put("glwsInfo", glwsInfo);
		
		String trnsId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> trnsInfo = handlerChain.get(trnsId).handle(inputStream);
		trnsInfo.put("id", trnsId);
		trnsInfo.put("desc", EffectObjTypeEnum.bm(trnsId));
		data.put("trnsInfo", trnsInfo);
		
		String inprId = ByteUtil.readByteToClassId(inputStream);
		Map<String, Object> inprInfo = handlerChain.get(inprId).handle(inputStream);
		inprInfo.put("id", inprId);
		inprInfo.put("desc", EffectObjTypeEnum.bm(inprId));
		data.put("inprInfo", inprInfo);
		
		return data;
	}

}
