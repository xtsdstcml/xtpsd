package top.xtcoder.xtpsd.core.layermask.handle.effect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 图层版本号
 * @author xiangtao
 *
 */
public class LayerEffectLyvrHandler implements ILayerEffectHandler{
	@Override
	public Map<String, Object> handle(byte[] effectData) throws IOException {
		try(InputStream inputStream = new ByteArrayInputStream(effectData)) {
			Map<String, Object> map = new HashMap<>();
			
			int version = ByteUtil.readByteToInt(inputStream, 4);
			map.put("version", version);
			return map;
		}
	}
}
