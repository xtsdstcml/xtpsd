package top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.base.bak;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeLongNoneValueHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeTextHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectFilterFXListHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectFilterFXListHandler.class);
	
	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(LayerEffectPlLdType.Nm.name(), new EffectTypeTextHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.blendOptions.name(), new EffectBlendOptionsHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.enab.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.hasoptions.name(), new EffectTypeBoolHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.FrgC.name(), new EffectFrgCHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.BckC.name(), new EffectBckCHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Fltr.name(), new EffectFltrHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.filterID.name(), new EffectTypeLongHandler());
		lfx2HandlerChain.put(LayerEffectPlLdType.Hght.name(), new EffectTypeDoubleHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			Map<String, Object> item = new HashMap<>();
			
			String objtype = ByteUtil.readByteToStr(inputStream, 4);
			item.put("type", objtype);
			String objName = ByteUtil.readByteToUnicodeStr(inputStream);
			item.put("name", objName);
			
			String objClassId = ByteUtil.readByteToClassId(inputStream);
			item.put("classId", objClassId);
			
			int propCount = ByteUtil.readByteToInt(inputStream, 4);
			item.put("propCount", propCount);
			if(i == 1) {
				propCount = propCount + 1;
			}
			List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
			for(int j = 0; j < propCount; j ++) {
				int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
				objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
				String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
				IEffectObjHandler hc = lfx2HandlerChain.get(objPropId);
				try {
					Map<String, Object> propertie = hc.handle(inputStream);
					propertie.put("id", objPropId);
					propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
					properties.add(propertie);
				}catch(Exception e) {
					String info = ByteUtil.readByteToStr(inputStream, inputStream.available());
					log.log(e, "id[%s]还未解析，后面一段字节的信息=%s", objPropId, info);
				}
			}
			
			item.put("properties", properties);
			list.add(item);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("count", count);
		map.put("list", list);
		return map;
	}
}
