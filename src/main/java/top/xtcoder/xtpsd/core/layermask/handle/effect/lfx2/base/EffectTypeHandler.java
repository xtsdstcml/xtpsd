package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;

/**
 * “渐变：渐变样式”为枚举型
 * @author xiangtao
 * 
 */
public class EffectTypeHandler extends EffectTypeEnumHandler{
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		if("Clry".equals(String.valueOf(map.get("enumId")))) {
			if("UsrS".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "用户颜色");
			}else if("FrgC".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "前景色");
			}else if("BckC".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "背景色");
			}
		}else if("GrdT".equals(String.valueOf(map.get("enumId")))) {
			if("Lnr".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "线性渐变");
			}else if("Rdl".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "径向渐变");
			}else if("Angl".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "角度渐变");
			}else if("Rflc".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "对称渐变");
			}else if("Dmnd".equals(String.valueOf(map.get("enumName")))) {
				map.put("label", "菱形渐变");
			}
		}
		return map;
	}
}
