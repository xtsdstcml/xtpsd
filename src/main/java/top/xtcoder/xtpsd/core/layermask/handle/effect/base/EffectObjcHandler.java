package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.PlLd.dto.LayerEffectPlLdType;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lang.EffectObjHandleChain;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * @author xiangtao
 *
 */
public class EffectObjcHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectObjcHandler.class);
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int nameLength = ByteUtil.readByteToInt(inputStream, 4);
		String name = ByteUtil.readByteToUnicodeStr(inputStream, nameLength * 2);
		
		int classIdLength = ByteUtil.readByteToInt(inputStream, 4);
		classIdLength = classIdLength < 4 ? 4 : classIdLength;
		String classId = ByteUtil.readByteToStr(inputStream, classIdLength);
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		
		for(int i = 0; i < count; i ++) {
			int objPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
			objPropIdLength = objPropIdLength < 4 ? 4 : objPropIdLength;
			String objPropId = ByteUtil.readByteToStr(inputStream, objPropIdLength);
			String type = ByteUtil.readByteToStr(inputStream, 4);
			IEffectObjHandler hc = EffectObjHandleChain.get(type);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("id", objPropId);
				propertie.put("type", type);
				propertie.put("desc", LayerEffectPlLdType.bm(objPropId));
				properties.add(propertie);
			}else {
				ByteUtil.viewStreamByteInfo(log, objPropId, type, inputStream);
				//没有的话后面就没必要解析了，直接扔掉
				while(inputStream.read() != -1);
				break;
			}
			if("BmpC".equals(objPropId)) {
				//BmpC后面这16字节不晓得是啥，丢弃
				String b = ByteUtil.readByteToStr(inputStream, 16);
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("nameLength", nameLength);
		map.put("name", name);
		map.put("classIdLength", classIdLength);
		map.put("classId", classId);
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
