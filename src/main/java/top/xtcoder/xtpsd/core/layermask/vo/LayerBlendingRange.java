package top.xtcoder.xtpsd.core.layermask.vo;

import java.util.List;
import java.util.Map;

public class LayerBlendingRange {
	/**
	 * 图层混合颜色带信息数据长度，
	 */
	private int layerBlendingRangeLength;
	/**
	 * 颜色通道数量 = （数据长度-8字节）/ 8
	 */
	private int colorChannelNumber;
	
	/**
	 * 图层混合颜色带信息中的本图层灰度-黑色
	 */
	private int compositeColorCurrentBlack;
	
	/**
	 * 图层混合颜色带信息中的本图层灰度-白色
	 */
	private int compositeColorCurrentWhite;
	
	/**
	 * 图层混合颜色带信息中的下一图层灰度-黑色
	 */
	private int compositeColorNextBlack;
	
	/**
	 * 图层混合颜色带信息中的下一图层灰度-白色
	 */
	private int compositeColorNextWhite;
	
	/**
	 * 图层混合颜色带各个颜色通道图层混合颜色带信息
	 */
	private List<Map<String, Integer>> compositeColors;

	private int totalDataSize;
	
	public int getLayerBlendingRangeLength() {
		return layerBlendingRangeLength;
	}

	public void setLayerBlendingRangeLength(int layerBlendingRangeLength) {
		this.layerBlendingRangeLength = layerBlendingRangeLength;
	}

	public int getColorChannelNumber() {
		return colorChannelNumber;
	}

	public void setColorChannelNumber(int colorChannelNumber) {
		this.colorChannelNumber = colorChannelNumber;
	}

	public int getCompositeColorCurrentBlack() {
		return compositeColorCurrentBlack;
	}

	public void setCompositeColorCurrentBlack(int compositeColorCurrentBlack) {
		this.compositeColorCurrentBlack = compositeColorCurrentBlack;
	}

	public int getCompositeColorCurrentWhite() {
		return compositeColorCurrentWhite;
	}

	public void setCompositeColorCurrentWhite(int compositeColorCurrentWhite) {
		this.compositeColorCurrentWhite = compositeColorCurrentWhite;
	}

	public int getCompositeColorNextBlack() {
		return compositeColorNextBlack;
	}

	public void setCompositeColorNextBlack(int compositeColorNextBlack) {
		this.compositeColorNextBlack = compositeColorNextBlack;
	}

	public int getCompositeColorNextWhite() {
		return compositeColorNextWhite;
	}

	public void setCompositeColorNextWhite(int compositeColorNextWhite) {
		this.compositeColorNextWhite = compositeColorNextWhite;
	}

	public List<Map<String, Integer>> getCompositeColors() {
		return compositeColors;
	}

	public void setCompositeColors(List<Map<String, Integer>> compositeColors) {
		this.compositeColors = compositeColors;
	}

	public int getTotalDataSize() {
		return totalDataSize;
	}

	public void setTotalDataSize(int totalDataSize) {
		this.totalDataSize = totalDataSize;
	}
}
