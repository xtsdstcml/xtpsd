package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeEnumHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;

/**
 * 结构：混合模式类型
 * @author xiangtao
 *
 */
public class EffectMdHandler extends EffectTypeEnumHandler{

	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		Map<String, Object> map = super.handle(inputStream);
		String enumName = (String) map.get("enumName");
		EffectObjTypeEnum mdType = EffectObjTypeEnum.bm(enumName);
		map.put("mdType", mdType);
		return map;
	}
}
