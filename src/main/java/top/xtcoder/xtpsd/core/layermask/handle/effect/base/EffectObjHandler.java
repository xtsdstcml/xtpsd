package top.xtcoder.xtpsd.core.layermask.handle.effect.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjClssHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjEnmrHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjIdntHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjIndxHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjNameHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjPropHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.obj.EffectObjReleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.lang.ObjPropType;
import top.xtcoder.xtpsd.log.Log;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 引用型对象
 * @author xiangtao
 *
 */
public class EffectObjHandler implements IEffectObjHandler{
	private static final Log log = Log.getLog(EffectObjHandler.class);
	
	protected static Map<String, IEffectObjHandler> handlerChain = new HashMap<>();
	
	static {
		handlerChain.put(ObjPropType.prop.key(), new EffectObjPropHandler());
		handlerChain.put(ObjPropType.Clss.key(), new EffectObjClssHandler());
		handlerChain.put(ObjPropType.Enmr.key(), new EffectObjEnmrHandler());
		handlerChain.put(ObjPropType.rele.key(), new EffectObjReleHandler());
		handlerChain.put(ObjPropType.Idnt.key(), new EffectObjIdntHandler());
		handlerChain.put(ObjPropType.indx.key(), new EffectObjIndxHandler());
		handlerChain.put(ObjPropType.name.key(), new EffectObjNameHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		int count = ByteUtil.readByteToInt(inputStream, 4);
		List<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
		for(int i = 0; i < count; i ++) {
			String type = ByteUtil.readByteToStr(inputStream, 4);
			IEffectObjHandler hc = handlerChain.get(type);
			if(hc != null) {
				Map<String, Object> propertie = hc.handle(inputStream);
				propertie.put("type", type);
				propertie.put("typeDesc", ObjPropType.bm(type));
				properties.add(propertie);
			}else {
				ByteUtil.viewStreamByteInfo(log, "obj", type, inputStream);
				while(inputStream.read() != -1);
				break;
			}
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("count", count);
		map.put("properties", properties);
		return map;
	}
}
