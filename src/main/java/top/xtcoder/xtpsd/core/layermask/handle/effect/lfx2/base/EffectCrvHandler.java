package top.xtcoder.xtpsd.core.layermask.handle.effect.lfx2.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeBoolHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.EffectTypeDoubleHandler;
import top.xtcoder.xtpsd.core.layermask.handle.effect.base.dto.EffectObjTypeEnum;
import top.xtcoder.xtpsd.core.layermask.handle.effect.baseinterface.IEffectObjHandler;
import top.xtcoder.xtpsd.utils.ByteUtil;

/**
 * 曲线附加信息
 * @author xiangtao
 *
 */
public class EffectCrvHandler implements IEffectObjHandler{

	protected static Map<String, IEffectObjHandler> lfx2HandlerChain = new HashMap<>();
	
	static {
		lfx2HandlerChain.put(EffectObjTypeEnum.Hrzn.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Vrtc.name(), new EffectTypeDoubleHandler());
		lfx2HandlerChain.put(EffectObjTypeEnum.Cnty.name(), new EffectTypeBoolHandler());
	}
	
	@Override
	public Map<String, Object> handle(InputStream inputStream) throws IOException {
		String type = ByteUtil.readByteToStr(inputStream, 4);
		int pointCount = ByteUtil.readByteToInt(inputStream, 4);
		
		//曲线点
		List<Map<String,Object>> points = new ArrayList<Map<String,Object>>();
		for(int i = 0; i < pointCount; i ++) {
			Map<String,Object> point = new HashMap<>();
			String pointObjType = ByteUtil.readByteToStr(inputStream, 4);
			
			int pointNameLength = ByteUtil.readByteToInt(inputStream, 4);
			String pointName = ByteUtil.readByteToStr(inputStream, pointNameLength * 2);
			
			int pointClassIdLength = ByteUtil.readByteToInt(inputStream, 4);
			pointClassIdLength = pointClassIdLength < 4 ? 4 : pointClassIdLength;
			String pointClassId = ByteUtil.readByteToStr(inputStream, pointClassIdLength);
			
			int propCount = ByteUtil.readByteToInt(inputStream, 4);
			
			//属性
			for(int j = 0; j < propCount; j ++) {
				int pointPropIdLength = ByteUtil.readByteToInt(inputStream, 4);
				pointPropIdLength = pointPropIdLength < 4 ? 4 : pointPropIdLength;
				String pointPropId = ByteUtil.readByteToStr(inputStream, pointPropIdLength);
				IEffectObjHandler handle = lfx2HandlerChain.get(pointPropId);
				
				if(handle != null) {
					Map<String, Object> propMaps = handle.handle(inputStream);
					propMaps.put("id", pointPropId);
					propMaps.put("desc", EffectObjTypeEnum.bm(pointPropId));
					point.put("props", propMaps);
				}else {
					System.out.println("EffectCrvHandler【" + pointPropId + "】not found");
				}
			}
			point.put("pointObjType", pointObjType);
			point.put("pointNameLength", pointNameLength);
			point.put("pointName", pointName);
			point.put("pointClassIdLength", pointClassIdLength);
			point.put("pointClassId", pointClassId);
			points.add(point);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("type", type);
		map.put("pointCount", pointCount);
		map.put("points", points);
		return map;
	}
}
