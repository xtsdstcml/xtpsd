package top.xtcoder.xtpsd.core;

import top.xtcoder.xtpsd.core.color.mode.data.vo.PsdColorModeData;
import top.xtcoder.xtpsd.core.file.header.vo.PsdFileHeader;
import top.xtcoder.xtpsd.core.image.data.PsdImageData;
import top.xtcoder.xtpsd.core.image.resources.vo.PsdImageResources;
import top.xtcoder.xtpsd.core.layermask.vo.PsdLayerAndMaskInfomation;

public class PsdInfo {
	private PsdFileHeader fileHeader;
	private PsdColorModeData colorModeData;
	private PsdImageResources imageResources;
	private PsdLayerAndMaskInfomation layerAndMaskInfomation;
	private PsdImageData imageData;
	
	public PsdFileHeader getFileHeader() {
		return fileHeader;
	}

	public void setFileHeader(PsdFileHeader fileHeader) {
		this.fileHeader = fileHeader;
	}

	public PsdColorModeData getColorModeData() {
		return colorModeData;
	}

	public void setColorModeData(PsdColorModeData colorModeData) {
		this.colorModeData = colorModeData;
	}

	public PsdImageResources getImageResources() {
		return imageResources;
	}

	public void setImageResources(PsdImageResources imageResources) {
		this.imageResources = imageResources;
	}

	public PsdLayerAndMaskInfomation getLayerAndMaskInfomation() {
		return layerAndMaskInfomation;
	}

	public void setLayerAndMaskInfomation(PsdLayerAndMaskInfomation layerAndMaskInfomation) {
		this.layerAndMaskInfomation = layerAndMaskInfomation;
	}

	public PsdImageData getImageData() {
		return imageData;
	}

	public void setImageData(PsdImageData imageData) {
		this.imageData = imageData;
	}

	@Override
	public String toString() {
		return "PsdInfo [fileHeader=" + fileHeader + ", colorModeData=" + colorModeData + ", imageResources="
				+ imageResources + ", layerAndMaskInfomation=" + layerAndMaskInfomation + ", imageData=" + imageData
				+ "]";
	}
}
