package top.xtcoder.xtpsd.core.color.mode.data.vo;

public class PsdColorModeData {
	private int colorDataLength;
	private byte[] colorData;
	
	public int getColorDataLength() {
		return colorDataLength;
	}
	public void setColorDataLength(int colorDataLength) {
		this.colorDataLength = colorDataLength;
	}
	public byte[] getColorData() {
		return colorData;
	}
	public void setColorData(byte[] colorData) {
		this.colorData = colorData;
	}
	
	
}
