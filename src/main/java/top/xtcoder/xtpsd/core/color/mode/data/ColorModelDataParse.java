package top.xtcoder.xtpsd.core.color.mode.data;

import java.io.FileInputStream;
import java.io.IOException;

import top.xtcoder.xtpsd.core.color.mode.data.vo.PsdColorModeData;
import top.xtcoder.xtpsd.utils.ByteUtil;

public class ColorModelDataParse {
	public PsdColorModeData parse(FileInputStream fis) throws IOException {
		PsdColorModeData colorModel = new PsdColorModeData();
		byte[] bsColorModelDataLength = new byte[4];
		fis.read(bsColorModelDataLength);
		int colorModelDataLength = ByteUtil.bytesToInt(bsColorModelDataLength);
		colorModel.setColorDataLength(colorModelDataLength);
		if(colorModelDataLength > 0) {
			byte[] colorData = new byte[colorModelDataLength];
			colorModel.setColorData(colorData);
		}	
		return colorModel;
	}
	
}
