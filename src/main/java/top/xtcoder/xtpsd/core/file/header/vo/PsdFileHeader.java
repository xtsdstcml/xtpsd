package top.xtcoder.xtpsd.core.file.header.vo;

import top.xtcoder.xtpsd.base.FileColorModel;

/**
 * PSD文件头
 * @author 向涛
 *
 */
public class PsdFileHeader {
	/**
	 * 签名：固定为'8BPS'，如果与此值不匹配，请不要尝试读取该文件。
	 */
	private String signature;
	
	/**
	 * 版本: PSD固定为1，PSB固定为2，如果与此值不匹配，请不要尝试读取该文件。
	 */
	private String version;
	
	/**
	 * 必须为0
	 */
	private int reserved;
	
	/**
	 * 图像中的通道数，包括任何的alpha通道，范围为1到56
	 */
	private int channelsNumber;
	
	/**
	 * 图像的高度，单位为像素，支持范围1-30000，PSB最高可位300000
	 */
	private int height;
	
	/**
	 * 图像的宽度，单位为像素，支持范围1-30000，PSB最高可位300000
	 */
	private int width;
	
	/**
	 * 深度：每个通道的位数，支持的值为1、8、16和32
	 */
	private int depth;
	
	/**
	 * 文件的颜色模式: 支持的值有Bitmap = 0; Grayscale = 1; Indexed = 2; RGB = 3; CMYK = 4; Multichannel = 7; Duotone = 8; Lab = 9.
	 */
	private FileColorModel colorModel;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getReserved() {
		return reserved;
	}

	public void setReserved(int reserved) {
		this.reserved = reserved;
	}

	public int getChannelsNumber() {
		return channelsNumber;
	}

	public void setChannelsNumber(int channelsNumber) {
		this.channelsNumber = channelsNumber;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public FileColorModel getColorModel() {
		return colorModel;
	}

	public void setColorModel(FileColorModel colorModel) {
		this.colorModel = colorModel;
	}

	@Override
	public String toString() {
		return "PsdFileHeader [signature=" + signature + ", version=" + version + ", reserved=" + reserved
				+ ", channelsNumber=" + channelsNumber + ", height=" + height + ", width=" + width + ", depth=" + depth
				+ ", colorModel=" + colorModel + "]";
	}
}
